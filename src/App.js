import React, { useEffect, useRef, useState } from "react";
import "./App.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./components/Layout";
import Home from "./pages/Home";
import About from "./pages/About";
import Store from "./pages/Store/Store";
import Blog from "./pages/Blog/Blog";
import Contact from "./pages/Contact/Contact";
import Wishlist from "./pages/Wishlist/Wishlist";
import Login from "./pages/Auth/Login";
import Signup from "./pages/Auth/Signup";
import Forgotpassword from "./pages/Auth/Forgotpassword";
import ResetPassword from "./pages/Auth/ResetPassword";
import SingleBlog from "./pages/SingleBlog/SingleBlog";
import PrivacyPolicy from "./pages/Policy/PrivacyPolicy";
import RefundPolicy from "./pages/Policy/RefundPolicy";
import ShippingPolicy from "./pages/Policy/ShippingPolicy";
import TermAndConditions from "./pages/Policy/TermAndConditions";
import SingleProduct from "./pages/SingleProduct/SingleProduct";
import Cart from "./pages/Cart/Cart";
import ScrollToTop from "./components/ScrollToTop";
import PayInfo from "./pages/PayInfo/PayInfo";
import UserInfo from "./pages/UserInfo/UserInfo";
import SocketClient from "./socket";

const App = () => {
  useEffect(() => {
    SocketClient();
  }, []);

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/contact" element={<Contact />} />
            <Route path="/products" element={<Store />} />
            <Route path="/products/:slug" element={<SingleProduct />} />
            <Route path="/blogs" element={<Blog />} />
            <Route path="/blogs/:id" element={<SingleBlog />} />
            <Route path="/wishlist" element={<Wishlist />} />
            <Route path="/login" element={<Login />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/forgot-password" element={<Forgotpassword />} />
            <Route path="/reset-password" element={<ResetPassword />} />
            <Route path="/privacy-policy" element={<PrivacyPolicy />} />
            <Route path="/refund-policy" element={<RefundPolicy />} />
            <Route path="/shipping-policy" element={<ShippingPolicy />} />
            <Route path="/term-conditions" element={<TermAndConditions />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/payment-info" element={<PayInfo />} />
            <Route path="/user-info" element={<UserInfo />} />
          </Route>
        </Routes>
        <ScrollToTop />
      </BrowserRouter>
    </>
  );
};

export default App;
