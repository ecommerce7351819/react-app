import { Button } from "antd";
import "./CusButton.scss";

const CusButton = (props) => {
  const { label, type, loading, className, ...rest } = props;

  const buttonType = type || "button";

  return (
    <Button
      className={`cus-button ${className}`}
      htmlType={buttonType}
      loading={loading}
      {...rest}
    >
      {label}
    </Button>
  );
};

export default CusButton;
