import React, { useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { formattedMoney } from "../config/helper";
import { Dropdown, Form } from "antd";
import CustomInput from "./CustomInput/CustomInput";
import CusButton from "./CusButton/CusButton";
import { SearchOutlined } from '@ant-design/icons';
import {
  HeartOutlined,
  UserOutlined,
  ShoppingCartOutlined,
  UnorderedListOutlined,
  PhoneOutlined
} from '@ant-design/icons';
import { getCart, getThisUser } from "../redux/features/authSlice";

const Header = () => {
  const dispatch = useDispatch()
  const { user, cart } = useSelector((state) => state.auth)
  const { categories } = useSelector((state) => state.category)
  const navigate = useNavigate()
  const [showCate, setShowCate] = useState(false)
  const location = useLocation()

  const logout = () => {
    localStorage.removeItem("accessToken")
    navigate("/")
    window.location.reload()
  }

  const items = [
    {
      key: '1',
      label: (
        <div onClick={() => logout()}>
          Đăng xuất
        </div>
      ),
    },
  ];

  const handleSearch = (value) => {
    if (value.keyword) {
      navigate(`/products?keyword=${value.keyword}`)
    }
  }

  const getUser = () => {
    dispatch(getThisUser())
    dispatch(getCart())
  }

  const queryParams = new URLSearchParams(location.search);

  const category = queryParams.get('category');


  const handleChooseCate = (name) => {
    if (name !== 'new') {
      if (category) {
        queryParams.set('category', name)
      } else {
        queryParams.append('category', name)
      }
      const newUrl = `products?${queryParams.toString()}`;
      navigate(newUrl);
    } else {
      navigate("blogs")
    }
  }

  useEffect(() => {
    getUser()
  }, [])

  return (
    <header className="header-upper py-2">
      <div className="container-xxl">
        <div className="row align-items-center">
          <div className="col-3 d-flex">
            <h2 className="mb-0">
              <Link to='/' className="text-white">Longstore</Link>
            </h2>
            <div className="dropdown-cate" onClick={() => setShowCate(!showCate)}>
              <div className="d-flex align-items-center gap-5 text-white">
                <UnorderedListOutlined className="fs-4" />
                <p className="mb-0">
                  Danh mục
                </p>
                <div className={`block-cate ${showCate ? '' : 'd-none'}`}>
                  {categories?.map((item, index) =>
                    <div onClick={() => handleChooseCate(item?.name)} className="cate" key={index}>{item.title}</div>
                  )}
                </div>
                <div className={`overlay ${showCate ? 'overlay-active' : ''}`}></div>
              </div>
            </div>
          </div>
          <div className="col-3">
            <Form onFinish={handleSearch} className="search">
              <CusButton type='submit' className='btn-search' icon={<SearchOutlined />} />
              <Form.Item className="mb-0" name="keyword">
                <CustomInput label="Tìm kiếm..." className='input-search' />
              </Form.Item>
            </Form>
          </div>
          <div className="col-6">
            <div className="header-upper-links d-flex align-items-center justify-content-end gap-15">
              <div>
                <Link
                  to="tel: 0848665855"
                  className="d-flex align-items-center gap-10 text-white"
                >
                  <PhoneOutlined className="fs-2" rotate={90} />
                  <p className="mb-0">
                    Gọi mua hàng
                  </p>
                </Link>
              </div>
              <div>
                <Link
                  to="wishlist"
                  className="d-flex align-items-center gap-10 text-white"
                >
                  <HeartOutlined className="fs-2" />
                  <p className="mb-0">
                    Yêu Thích
                  </p>
                </Link>
              </div>
              <div>
                {user
                  ?
                  <Dropdown menu={{ items }}>
                    <Link
                      to="/user-info"
                      className="d-flex align-items-center gap-10 text-white"
                    >
                      <UserOutlined className="fs-2" />
                      <p className="mb-0">
                        {user.firstName} <br /> {user.lastName}
                      </p>
                    </Link>
                  </Dropdown>
                  : <Link
                    to="login"
                    className="d-flex align-items-center gap-10 text-white"
                  >
                    <img src="../images/user.svg" alt="user" />
                    <p className="mb-0">
                      Đăng Nhập <br /> Tài Khoản
                    </p>
                  </Link>
                }
              </div>
              <div>
                <Link
                  to="/cart"
                  className="d-flex align-items-center gap-10 text-white"
                >
                  <ShoppingCartOutlined className="fs-2" />
                  <div className="d-flex flex-column">
                    <span className="badge bg-white text-dark">{cart ? cart.quantityTotal : 0}</span>
                    <p className="mb-0 text-center">{formattedMoney(cart?.cartTotal)}₫</p>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
