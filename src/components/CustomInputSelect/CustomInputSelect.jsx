import { Select } from "antd";

const CustomInputSelect = ({ className, ...props }) => {
  return (
    <Select
      className={`cus-select ${className}`}
      style={{ width: 240 }}
      {...props}
      showSearch
      filterOption={(input, option) => (option?.label ?? '').includes(input)}
      filterSort={(optionA, optionB) =>
        (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
      }
    />
  );
};

export default CustomInputSelect;
