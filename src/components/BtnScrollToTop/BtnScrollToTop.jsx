import React from 'react'
import { useEffect, useState } from 'react'
import './BtnScrollToTop.scss'
import { ArrowUpOutlined } from '@ant-design/icons';

const BtnScrollToTop = () => {

  const [showBtn, setShowBtn] = useState()

  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.scrollY > 300) {
        setShowBtn(true)
      } else {
        setShowBtn(false)
      }
    })
  }, [])

  const handleScrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    })
  }

  return (
    <>
      {showBtn &&
        <div className='btn-scroll-to-top' onClick={() => handleScrollToTop()}>
          <ArrowUpOutlined />
        </div>
      }
    </>
  )
}

export default BtnScrollToTop
