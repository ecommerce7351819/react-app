import React from 'react'

const Loading = (prod) => {

  const { isLoading } = prod

  return (
    <div className={`loader ${isLoading ? '' : 'd-none'}`}>
      <span className='rotating'></span>
    </div>
  )
}

export default Loading
