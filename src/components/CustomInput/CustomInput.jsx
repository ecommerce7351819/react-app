import { Input } from "antd";
import "./CustomInput.scss";

const CustomInput = (props) => {
  const { type, label, i_id, i_class, isSearch, ...rest } = props;

  return (
    <Input
      type={type}
      id={i_id}
      placeholder={label}
      classNames={`custom-input ${i_class}`}
      {...rest}
    />
  );
};

export default CustomInput;
