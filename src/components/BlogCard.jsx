import React from "react";
import { Link } from "react-router-dom";

const BlogCard = (props) => {
  const { data } = props;
  return (
    <Link to={`/blogs/${data._id}`}>
      <div className="blog-card boxshadow">
        <div className="card-image">
          <img className="img-fluid w-100" src={data?.img?.url} alt="" />
        </div>
        <div className="blog-content">
          <p className="date">{data?.createdAt}</p>
          <h5 className="title">{data?.title}</h5>
          <p className="desc">
          </p>
          <span className="btn-link-to">
            Xem Thêm
          </span>
        </div>
      </div>
    </Link>
  );
};

export default BlogCard;
