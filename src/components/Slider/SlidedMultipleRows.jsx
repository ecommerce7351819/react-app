import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import './CusSlide.scss'

const SlidedMultipleRows = (props) => {

  const { row, slidesToShow, autoplay, dots, infinite } = props

  const settings = {
    infinite: infinite,
    speed: 500,
    slidesToShow: slidesToShow ? slidesToShow : 5,
    slidesToScroll: 1,
    swipeToSlide: true,
    rows: row ? row : 1,
    autoplay: autoplay,
    dots: dots,
  };
  return (
    <Slider {...settings}>
      {props.children}
    </Slider>
  );
}

export default SlidedMultipleRows