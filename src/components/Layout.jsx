import { Outlet } from 'react-router-dom'
import Footer from './Footer'
import Header from './Header'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import BtnScrollToTop from './BtnScrollToTop/BtnScrollToTop';
import { useSelector } from 'react-redux';
import Loading from './Loading';

const Layout = () => {
	const { isLoading } = useSelector(state => state.auth)
	const { isLoadingBanner } = useSelector(state => state.banner)

	return (
		<>
			<Loading isLoading={isLoading && isLoadingBanner} />
			<Header />
			<Outlet />
			<BtnScrollToTop />
			<Footer />
			<ToastContainer
				position="top-center"
				autoClose={5000}
				hideProgressBar={false}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss
				draggable
				pauseOnHover
				theme="colored"
			/>
		</>
	)
}

export default Layout
