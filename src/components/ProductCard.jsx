import React, { useEffect, useState } from "react";
import ReactStars from "react-rating-stars-component";
import { Link } from "react-router-dom";
import { formattedMoney } from "../config/helper";
import { useDispatch, useSelector } from "react-redux";
import { addToWishlist } from "../redux/features/authSlice";
import { toast } from "react-toastify";

const ProductCard = (props) => {
  const { data } = props
  const dispatch = useDispatch()
  const [isInWishlist, setIsInWishlist] = useState(false)

  const { user } = useSelector(state => state.auth)

  const handleAddToWishlist = (id) => {
    if (user) {
      dispatch(addToWishlist(id))
    } else {
      toast.error("Bạn phải đăng nhập thì mới dùng được danh sách yêu thích!")
    }
  }

  const checkWishlist = () => {
    if (user?.wishlist?.includes(data?._id)) {
      setIsInWishlist(true)
    } else {
      setIsInWishlist(false)
    }
  }

  useEffect(() => {
    checkWishlist()
  }, [user])

  return (
    <div className="product-card boxshadow">
      <Link
        to={`/products/${data?.slug}`}
        className="mb-2"
      >
        <div className="d-flex justify-content-center mt-2 mb-3">
          <img className="product-img" src={data?.mainImg?.url} alt="" />
        </div>
        <div className="product-name">
          <h3>{data?.title}</h3>
        </div>
        <div className="product-price">
          <p className="product-price__show">{formattedMoney(data?.price - data?.price * (data?.discount / 100))}₫</p>
          <p className="product-price__through">{formattedMoney(data?.price)}₫</p>
          <div className="product-price-percent">
            <p className="product-price-percent__detail">Giảm {data?.discount}%</p>
          </div>
        </div>
        <ReactStars
          count={5}
          size={20}
          activeColor="#f59e0b"
          isHalf={true}
          emptyIcon={<i className="far fa-star" />}
          halfIcon={<i className="far fa-star-half-alt" />}
          fullIcon={<i className="far fa-star" />}
          value={data?.totalRating}
          edit={false}
        />
      </Link>
      <div className="add-wishlist" onClick={() => handleAddToWishlist(data?._id)}>
        <span>Yêu thích</span>
        <div>
          {isInWishlist ? <img src="images/wish-black.svg" alt="" /> : <img src="images/wish.svg" alt="" />}
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
