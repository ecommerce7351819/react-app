import React from "react";
import {
  BsFacebook,
  BsGithub,
  BsInstagram,
  BsLinkedin,
  BsYoutube,
} from "react-icons/bs";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <>
      <footer className="py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-4">
              <h4 className="mb-4">Liên Hệ Với Chúng Tôi</h4>
              <div>
                <address className="fs-6">
                  Tòa nhà Sông Đà, Phạm Hùng, <br />
                  Mỹ Đình, Nam Từ Liêm, Hà Nội
                </address>
                <a
                  href="tel: 0848665855"
                  className="mt-3 d-block mb-1"
                >
                  0848665855
                </a>
                <a
                  href="mailto: phamtrieulong2001@gmail.com"
                  className="mt-2 d-block mb-0"
                >
                  phamtrieulong2001@gmail.com
                </a>
                <div className="social_icons d-flex align-items-center gap-30 mt-4">
                  <a href="/">
                    <BsLinkedin className="fs-4" />
                  </a>
                  <a href="/">
                    <BsInstagram className="fs-4" />
                  </a>
                  <a href="/">
                    <BsGithub className="fs-4" />
                  </a>
                  <a href="/">
                    <BsYoutube className="fs-4" />
                  </a>
                  <a href="/">
                    <BsFacebook className="fs-4" />
                  </a>
                </div>
              </div>
            </div>
            <div className="col-3">
              <h4 className="mb-4">Thông Tin</h4>
              <div className="footer-links d-flex flex-column">
                <Link to="/privacy-policy" className="py-2 mb-1">
                  Chính Sách Bảo Mật
                </Link>
                <Link to="/refund-policy" className="py-2 mb-1">
                  Chính Sách Hoàn Tiền
                </Link>
                <Link to="/shipping-policy" className="py-2 mb-1">
                  Chính Sách Vận Chuyển
                </Link>
                <Link to="/term-conditions" className="py-2 mb-1">
                  Điều Khoản Dịch Vụ
                </Link>
                <Link to="/blogs" className="py-2 mb-1">
                  Blogs
                </Link>
              </div>
            </div>
            <div className="col-3">
              <h4 className="mb-4">Tài Khoản</h4>
              <div className="footer-links d-flex flex-column">
                <Link className="py-2 mb-1">Tìm Kiếm</Link>
                <Link className="py-2 mb-1">Về Chúng Tôi</Link>
                <Link className="py-2 mb-1">Câu Hỏi Thường Gặp</Link>
                <Link className="py-2 mb-1">Liên Hệ</Link>
                <Link className="py-2 mb-1">Size Chart</Link>
              </div>
            </div>
            <div className="col-2">
              <h4 className="mb-4">Liên Kết Nhanh</h4>
              <div className="footer-links d-flex flex-column">
                <Link className="py-2 mb-1">Phụ Kiện</Link>
                <Link className="py-2 mb-1">Laptops</Link>
                <Link className="py-2 mb-1">Tai Nghe</Link>
                <Link className="py-2 mb-1">Smart Watches</Link>
                <Link className="py-2 mb-1">Tablets</Link>
              </div>
            </div>
          </div>
        </div>
      </footer >
    </>
  );
};

export default Footer;
