import React, { useEffect } from 'react'
import "./BannerZone.scss"
import { Carousel, Col, Row } from 'antd'
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { getMainBanners, getSubBanners } from '../../redux/features/bannerSlice';

const BannerZone = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch();


  const getBanners = async () => {
    dispatch(getSubBanners())
    dispatch(getMainBanners())
  }

  useEffect(() => {
    getBanners();
  }, [])

  const handleNagivate = (link) => {
    navigate(`${link}`)
  }

  const { mainBanner, subBanner } = useSelector(state => state.banner)

  return (
    <div className='banner-zone home-wrapper-2 py-3'>
      <div className='container-xxl'>
        <Row className='mt-2'>
          <Col span={18}>
            <Carousel autoplay dots={false}>
              {mainBanner?.map((item, index) =>
                <div key={index}>
                  <img onClick={() => handleNagivate(item?.action)} className='img-main-banner' src={item?.img.url} alt='' />
                </div>
              )}
            </Carousel>
          </Col>
          <Col span={6}>
            {subBanner?.map((item, index) =>
              <div key={index}>
                <img onClick={() => handleNagivate(item?.action)} className='img-sub-banner' src={item?.img.url} alt="" />
              </div>
            )}
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default BannerZone
