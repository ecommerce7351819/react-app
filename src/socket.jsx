import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { io } from "socket.io-client";
import { getCart, getThisUser } from "./redux/features/authSlice";

const URL = "http://localhost:8888"; // URL của server Socket.io

const SocketClient = () => {
  const user = useSelector((state) => state.auth); // Lấy thông tin user từ Redux
  const dispatch = useDispatch();

  useEffect(() => {
    if (user) {
      const socket = io.connect(URL);

      socket.emit("join-room", user._id);
      console.log(user._id);

      // Lắng nghe sự kiện cập nhật giỏ hàng từ server
      socket.on("cart-updated", () => {
        dispatch(getCart())
      });

      return () => {
        socket.disconnect();
      };
    }
  }, [user, dispatch]);

  return null; // hoặc JSX của bạn
};

export default SocketClient;

