export const BASE_API_URL = "http://localhost:8888/api/";
export const API_URL = "https://longstoreapi.onrender.com/api";

export const WARRANTY = [
  {
    id: 1,
    title:
      "1 đổi 1 VIP 6 tháng: Đổi máy mới tương đương khi có lỗi từ NSX trong 6 tháng",
    percent: 5,
  },
  {
    id: 2,
    title:
      "S24+ 12 tháng: Đổi sản phẩm tương đương hoặc miễn phí chi phí sửa chữa nếu có lỗi của NSX khi hết hạn bảo hành trong 12 tháng",
    percent: 6,
  },
  {
    id: 3,
    title:
      "1 đổi 1 VIP 12 tháng: Đổi máy mới tương đương khi có lỗi từ NSX trong 12 tháng",
    percent: 7,
  },
  {
    id: 4,
    title:
      "Rơi vỡ - Rớt nước: Hỗ trợ 90% chi phí sửa chữa, đổi mới sản phẩm nếu hư hỏng nặng trong 12 tháng",
    percent: 8,
  },
];
