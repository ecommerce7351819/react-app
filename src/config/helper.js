export const formattedMoney = (money) => {
  return money ? money.toLocaleString() : 0;
};

export const formatISODateToYYYYMMDD = (isoDateStr) => {
  const isoDateRegex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/;

  if (isoDateRegex.test(isoDateStr)) {
    const isoDate = new Date(isoDateStr);
    const formattedDate = isoDate.toISOString().split("T")[0];
    return formattedDate;
  } else {
    return null;
  }
};
