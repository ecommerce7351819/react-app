import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./features/authSlice";
import productReducer from "./features/productSlice";
import categoryReducer from "./features/categorySlice";
import brandReducer from "./features/brandSlice";
import orderReducer from "./features/orderSlice";
import bannerReducer from "./features/bannerSlice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    product: productReducer,
    category: categoryReducer,
    brand: brandReducer,
    order: orderReducer,
    banner: bannerReducer,
  },
});
