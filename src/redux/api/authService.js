import axios from "../../config/axiosConfig";

const login = async (data) => {
  const res = await axios.post("user/login", data);
  localStorage.setItem("accessToken", res.data.token);
  return res.data;
};

const register = async (data) => {
  const res = await axios.post("user/register", data);
  return res.data;
};

const logout = async () => {};

const getThisUser = async () => {
  const res = await axios.get("user/this-user");
  return res.data;
};

const getWishlist = async () => {
  const res = await axios.get("user/wishlist");
  return res.data;
};

const addToCart = async (data) => {
  const res = await axios.put("user/add-to-cart", data);
  return res.data;
};

const getCart = async () => {
  const res = await axios.get("user/cart");
  return res.data;
};

const emptyCart = async () => {
  const res = await axios.put("user/empty-cart");
  return res.data;
};

const addAddress = async (data) => {
  const res = await axios.put("user/save-address", data);
  return res.data;
};

const updateUser = async (data) => {
  const res = await axios.put("user/update-user", data);
  return res.data;
};

const authService = {
  login,
  register,
  logout,
  getThisUser,
  getWishlist,
  addToCart,
  getCart,
  addAddress,
  emptyCart,
  updateUser,
};

export default authService;
