import axios from "../../config/axiosConfig";

const getHotProduct = async (category, count) => {
  try {
    const res = await axios.get(
      `/product/hot-product?category=${category}&count=${count}`
    );
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const getAllProducts = async (data) => {
  try {
    const queryParameters = new URLSearchParams({
      page: data.page,
      ...(data.keyword && { keyword: data.keyword }),
      ...(data.category && { category: data.category }),
      ...(data.brand && { brand: data.brand }),
      ...(data.sort && { sort: data.sort }),
    }).toString();
    const res = await axios.get(`/product?${queryParameters}`);
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const getAProduct = async (slug) => {
  try {
    const res = await axios.get(`/product/get-product/${slug}`);
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const addToWishlist = async (id) => {
  try {
    const res = await axios.put(`/product/wishlist`, { proId: id });
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const rating = async (data) => {
  try {
    const res = await axios.put(`/product/rating`, data);
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

export const productService = {
  getHotProduct,
  getAllProducts,
  getAProduct,
  addToWishlist,
  rating,
};
