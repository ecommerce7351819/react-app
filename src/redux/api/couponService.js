import axios from "../../config/axiosConfig";

const checkCoupon = async (data) => {
  try {
    const res = await axios.post("coupon/check-coupon", data);
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const couponService = { checkCoupon };

export default couponService;
