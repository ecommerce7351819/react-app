import axios from "../../config/axiosConfig";

const getAllBlogs = async () => {
  try {
    const res = await axios.get("blog/list");
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const getABlog = async (id) => {
  try {
    const res = await axios.get(`blog//get-blog/${id}`);
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const blogService = { getAllBlogs, getABlog };

export default blogService;
