import axios from "../../config/axiosConfig";

const createOrder = async (data) => {
  const res = await axios.post("order/create", data);
  return res.data;
};

const getAllOrder = async () => {
  const res = await axios.get("order/orders");
  return res.data;
};

const createPayment = async (data) => {
  await axios.post("pay/create-payment-url", data);
};

const orderService = { createOrder, getAllOrder, createPayment };

export default orderService;
