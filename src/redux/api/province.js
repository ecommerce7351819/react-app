const URL_PROVINCE = 'https://vapi.vnappmob.com'

const getListProvince = async () => {
  try {
    const res = await fetch(`${URL_PROVINCE}/api/province`).then((res) => res.json())
    return res.results
  } catch (error) {
    console.log(error);
  }
}

const getListDistrict = async (id) => {
  try {
    const res = await fetch(`${URL_PROVINCE}/api/province/district/${id}`).then((res) => res.json())
    return res.results
  } catch (error) {
    console.log(error);
  }
}

const getListWard = async (id) => {
  try {
    const res = await fetch(`${URL_PROVINCE}/api/province/ward/${id}`).then((res) => res.json())
    return res.results
  } catch (error) {
    console.log(error);
  }
}

const provinceService = {getListProvince, getListDistrict, getListWard} 

export default provinceService