import axios from '../../config/axiosConfig';

const getAllCategories = async () => {
  const res = await axios.get('category/list')
  return res.data
}

const categoryService = {getAllCategories}

export default categoryService