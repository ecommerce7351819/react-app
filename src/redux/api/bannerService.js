import axios from "../../config/axiosConfig";

const getMainBanners = async () => {
  try {
    const res = await axios.get("banner/get-main-banners");
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const getSubBanners = async () => {
  try {
    const res = await axios.get("banner/get-sub-banners");
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const getAllBanners = async () => {
  try {
    const res = await axios.get("banner/list");
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

const bannerService = { getMainBanners, getSubBanners, getAllBanners };

export default bannerService;
