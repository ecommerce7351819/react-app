import axios from "../../config/axiosConfig";

const getAllBrand = async () => {
  const res = await axios.get("brand/list");
  return res.data;
};

const brandService = { getAllBrand };

export default brandService;
