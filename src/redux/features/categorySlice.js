import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import categoryService from '../api/categoryService'

export const getAllCategories = createAsyncThunk("category/getAll", async(data, thunkAPI) => {
  try {
    const res = await categoryService.getAllCategories();
    return res
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

const initialState = {
  categories: null,
  isLoading: false,
  isError: false,
  isSuccess: false,
  message: ""
}

const categorySlice = createSlice({
  name: "category",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllCategories.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllCategories.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.categories = action.payload
        })
      .addCase(getAllCategories.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
      })
  }
})

export default categorySlice.reducer