import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import authService from "../api/authService";
import { toast } from "react-toastify";
import { productService } from "../api/productService";
import { socket } from "../../socket";

export const registerUser = createAsyncThunk(
  "user/register",
  async (data, thunkAPI) => {
    try {
      const res = await authService.register(data);
      return res;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getThisUser = createAsyncThunk(
  "user/get-this",
  async (data, thunkAPI) => {
    try {
      const res = await authService.getThisUser();
      return res;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const loginUser = createAsyncThunk(
  "user/login",
  async (data, thunkAPI) => {
    try {
      const res = await authService.login(data);
      return res;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getWishlist = createAsyncThunk(
  "user/wishlist",
  async (data, thunkAPI) => {
    try {
      const res = await authService.getWishlist();
      return res;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const addToWishlist = createAsyncThunk(
  "product/add-to-wishlist",
  async (data, thunkAPI) => {
    try {
      const res = await productService.addToWishlist(data);
      if (res?.message === "Remove successfully") {
        toast.error("Đã loại sản phẩm ra khỏi danh sách yêu thích");
      }
      if (res?.message === "Success") {
        toast.success("Đã thêm vào danh sách yêu thích!");
      }
      return res?.user;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const addToCart = createAsyncThunk(
  "user/add-to-cart",
  async (data, thunkAPI) => {
    try {
      const res = await authService.addToCart(data);
      return res;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getCart = createAsyncThunk(
  "user/get-cart",
  async (_, thunkAPI) => {
    try {
      const res = await authService.getCart();
      return res;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const addAddress = createAsyncThunk(
  "user/add-address",
  async (data, thunkAPI) => {
    try {
      const res = await authService.addAddress(data);
      return res;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const initialState = {
  user: null,
  wishlist: null,
  cart: null,
  isLoading: false,
  isError: false,
  isSuccess: false,
  message: "",
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(registerUser.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(registerUser.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.createdUser = action.payload;
        if (state.isSuccess === true) {
          toast.success("Đã tạo tài khoản thành công!!!");
        }
      })
      .addCase(registerUser.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
        if (state.isError === true) {
          toast.info(action.payload);
        }
      })

      .addCase(loginUser.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(loginUser.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.user = action.payload;
        state.message = "Success";
        if (state.isSuccess === true) {
          toast.success("Đăng nhập thành công!!!");
        }
      })
      .addCase(loginUser.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
        if (state.isError === true) {
          toast.info(action.payload);
        }
      })

      .addCase(getThisUser.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getThisUser.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.user = action.payload;
      })
      .addCase(getThisUser.rejected, (state, action) => {
        state.isLoading = false;
      })

      .addCase(getWishlist.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getWishlist.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.wishlist = action.payload;
      })

      .addCase(addToWishlist.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.user = action.payload;
      })

      .addCase(addToCart.fulfilled, (state, action) => {
        state.cart = action.payload;
        toast.success("Thêm vào giỏ hàng thành công!");
      })
      .addCase(getCart.fulfilled, (state, action) => {
        state.cart = action.payload;
      })
      .addCase(addAddress.fulfilled, (state, action) => {
        state.user = action.payload;
      });
  },
});

export default authSlice.reducer;
