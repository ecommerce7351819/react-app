import { createSlice } from "@reduxjs/toolkit";

const initialState = { info: null, coupon: null };

const orderSlice = createSlice({
  name: "order",
  initialState,
  reducers: {
    saveInfoOrder(state, action) {
      state.info = action.payload;
    },
    saveCoupon(state, action) {
      state.coupon = action.payload;
    },
  },
});

export const { saveInfoOrder, saveCoupon } = orderSlice.actions;

export default orderSlice.reducer;
