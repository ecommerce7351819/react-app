import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import bannerService from "../api/bannerService";

export const getMainBanners = createAsyncThunk(
  "banner/getMainBanners",
  async (_, thunkAPI) => {
    try {
      const res = await bannerService.getMainBanners();
      return res;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getSubBanners = createAsyncThunk(
  "banner/getSubBanners",
  async (_, thunkAPI) => {
    try {
      const res = await bannerService.getSubBanners();
      return res;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const initialState = {
  mainBanner: null,
  subBanner: null,
  isLoadingBanner: false,
  isError: false,
  isSuccess: false,
  message: "",
};

const bannerSlice = createSlice({
  name: "brand",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getMainBanners.pending, (state) => {
        state.isLoadingBanner = true;
      })
      .addCase(getMainBanners.fulfilled, (state, action) => {
        state.isLoadingBanner = false;
        state.isSuccess = true;
        state.mainBanner = action.payload;
      })
      .addCase(getMainBanners.rejected, (state, action) => {
        state.isLoadingBanner = false;
        state.isError = true;
        state.message = action.payload;
      })
      .addCase(getSubBanners.pending, (state) => {
        state.isLoadingBanner = true;
      })
      .addCase(getSubBanners.fulfilled, (state, action) => {
        state.isLoadingBanner = false;
        state.isSuccess = true;
        state.subBanner = action.payload;
      })
      .addCase(getSubBanners.rejected, (state, action) => {
        state.isLoadingBanner = false;
        state.isError = true;
        state.message = action.payload;
      });
  },
});

export default bannerSlice.reducer;
