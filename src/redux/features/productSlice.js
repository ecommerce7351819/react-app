import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { productService } from '../api/productService';

export const getHotMobile = createAsyncThunk("hot-product/mobile", async(count, thunkAPI) => {
  try {
    const res = await productService.getHotProduct("mobile", count);
    return res
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const getHotLaptop = createAsyncThunk("hot-product/laptop", async(count, thunkAPI) => {
  try {
    const res = await productService.getHotProduct('laptop', count);
    return res
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

export const getAllProducts = createAsyncThunk("product/getAll", async(data, thunkAPI) => {
  try {
    const res = await productService.getAllProducts(data);
    return res
  } catch (error) {
    return thunkAPI.rejectWithValue(error)
  }
})

const initialState = {
  hotMobile: null,
  hotLaptop: null,
  products: null,
  isLoading: false,
  isError: false,
  isSuccess: false,
  message: ""
}

const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getHotMobile.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getHotMobile.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.hotMobile = action.payload
      })
      .addCase(getHotMobile.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
      })
      .addCase(getHotLaptop.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getHotLaptop.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.hotLaptop = action.payload
      })
      .addCase(getHotLaptop.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
      })
      .addCase(getAllProducts.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllProducts.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.products = action.payload
      })
      .addCase(getAllProducts.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
      })
  }
})

export default productSlice.reducer