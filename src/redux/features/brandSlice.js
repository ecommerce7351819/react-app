import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import brandService from "../api/brandService";

export const getAllBrand = createAsyncThunk(
  "brand/getAll",
  async (_, thunkAPI) => {
    try {
      const res = await brandService.getAllBrand();
      return res;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const initialState = {
  brands: null,
  isLoading: false,
  isError: false,
  isSuccess: false,
  message: "",
};

const brandSlice = createSlice({
  name: "brand",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllBrand.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAllBrand.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.brands = action.payload;
      })
      .addCase(getAllBrand.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      });
  },
});

export default brandSlice.reducer;
