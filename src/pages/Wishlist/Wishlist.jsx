import React, { useEffect } from "react";
import "./Wishlist.scss";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import Meta from "../../components/Meta";
import ProductCard from "../../components/ProductCard";
import { useDispatch, useSelector } from "react-redux";
import { getWishlist } from "../../redux/features/authSlice";
import { Empty } from "antd";

const Wishlist = () => {

  const dispatch = useDispatch()

  const { user } = useSelector(state => state.auth)

  useEffect(() => {
    if (user) {
      dispatch(getWishlist())
    }
  }, [])

  const { wishlist } = useSelector(state => state.auth)

  return (
    <>
      <Meta title="Yêu thích" />
      <BreadCrumb title="Yêu thích" />
      <div className="wishlist-wrapper home-wrapper-2 py-4">
        <div className="container-xxl">
          {wishlist?.length > 0
            ?
            <div className="d-flex gap-10 flex-wrap">
              {wishlist.map((item, index) =>
                <div key={index}>
                  <ProductCard data={item} />
                </div>
              )}
            </div>
            :
            <Empty />
          }
        </div>
      </div>
    </>
  );
};

export default Wishlist;
