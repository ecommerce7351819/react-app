import React, { useEffect, useState } from "react";
import "./SingleBlog.scss";
import Meta from "../../components/Meta";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import { Link, useParams } from "react-router-dom";
import { HiOutlineArrowLeft } from "react-icons/hi";
import blogService from "../../redux/api/blogService";

const SingleBlog = () => {
  const { id } = useParams();
  const [blog, setBlog] = useState()

  const getBlog = async () => {
    const res = await blogService.getABlog(id)
    setBlog(res);
  }

  useEffect(() => {
    getBlog()
  }, [])

  return (
    <>
      <Meta title={"Name Blog"} />
      <BreadCrumb title="Name Blog" />
      <div className="blog-wrapper home-wrapper-2 py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="single-blog-card">
                <Link to="/blogs" className="d-flex align-item-center">
                  <HiOutlineArrowLeft className="fs-5" />
                  <span className="mx-1">Quay lại</span>
                </Link>
                <h1>{blog?.title}</h1>
                <div dangerouslySetInnerHTML={{ __html: blog?.description }}></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SingleBlog;
