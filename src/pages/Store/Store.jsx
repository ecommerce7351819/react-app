import React, { useEffect, useRef, useState } from "react";
import "./Store.scss";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import Meta from "../../components/Meta";
import ProductCard from "../../components/ProductCard";
import { Col, Empty, Pagination, Row } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getAllProducts } from "../../redux/features/productSlice";
import { useLocation, useNavigate } from "react-router-dom";
import { getAllCategories } from "../../redux/features/categorySlice";
import Loading from "../../components/Loading";
import { getAllBrand } from "../../redux/features/brandSlice";
import CusButton from "../../components/CusButton/CusButton";

const Store = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate();
  const location = useLocation()
  const [page, setPage] = useState(1)
  const toFirstProd = useRef(null);

  const handleScrollToFirstProd = () => {
    toFirstProd.current?.scrollIntoView({ behavior: "smooth" })
  }

  const changePage = (e) => {
    handleScrollToFirstProd()
    setPage(e);
  }

  const queryParams = new URLSearchParams(location.search);

  const keyword = queryParams.get('keyword');
  const brand = queryParams.get('brand');
  const category = queryParams.get('category');
  const sort = queryParams.get('sort');

  const addCate = (value) => {
    handleScrollToFirstProd()
    setPage(1)
    if (category) {
      queryParams.set('category', value.name)
    } else {
      queryParams.append('category', value.name)
    }
    const newUrl = `${location.pathname}?${queryParams.toString()}`;
    navigate(newUrl);
  }

  const addBrand = (value) => {
    handleScrollToFirstProd()
    setPage(1)
    if (brand) {
      queryParams.set('brand', value.title)
    } else {
      queryParams.append('brand', value.title)
    }
    const newUrl = `${location.pathname}?${queryParams.toString()}`;
    navigate(newUrl);
  }

  const handleChangeSort = (e) => {
    handleScrollToFirstProd()
    setPage(1)
    if (sort) {
      queryParams.set('sort', e.target.value)
    } else {
      queryParams.append('sort', e.target.value)
    }
    const newUrl = `${location.pathname}?${queryParams.toString()}`;
    navigate(newUrl);
  }

  const getBrands = async () => {
    await dispatch(getAllBrand());
  }

  useEffect(() => {
    queryParams.set('sort', 'sold-descending')
    const newUrl = `${location.pathname}?${queryParams.toString()}`;
    navigate(newUrl);
    getBrands()
  }, [])

  useEffect(() => {
    dispatch(getAllProducts({ page, keyword, brand, category, sort }))
    dispatch(getAllCategories())
  }, [page, keyword, brand, category, sort])

  const { categories } = useSelector(state => state.category)
  const { brands } = useSelector(state => state.brand)
  const { products, isLoading } = useSelector(state => state.product)

  const handleClear = () => {
    handleScrollToFirstProd()
    setPage(1)
    queryParams.delete("category")
    queryParams.delete("brand")
    queryParams.delete("keyword")
    const newUrl = `${location.pathname}?${queryParams.toString()}`;
    navigate(newUrl);
  }

  return (
    <>
      <Meta title="Sản Phẩm" />
      <BreadCrumb title="Sản Phẩm" />
      <div className="store-wrapper home-wrapper-2 py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-3">
              <div className="po-sticky">
                <div className="filter-card mb-3 boxshadow">
                  <h3 className="filter-title mb-1">Danh Mục</h3>
                  <div className="mx-2">
                    <ul className="ps-0">
                      {categories && categories.map((item, index) => <li onClick={() => addCate(item)} key={index}>{item?.title}</li>)}
                    </ul>
                  </div>
                </div>
                <div className="filter-card mb-3 boxshadow">
                  <h3 className="filter-title mb-1">Nhãn Hàng</h3>
                  <div className="mx-2">
                    <ul className="ps-0">
                      {brands && brands.map((item, index) => <li onClick={() => addBrand(item)} key={index}>{item?.title}</li>)}
                    </ul>
                  </div>
                </div>
              </div>

            </div>
            <div ref={toFirstProd} className="col-9">
              <div className="filter-sort-grid boxshadow">
                <div className="d-flex align-items-center justify-content-between">
                  <div className="d-flex align-items-center gap-10">
                    <p className="mb-0" style={{ width: "210px" }}>
                      Sắp Xếp Theo:{" "}
                    </p>
                    <select value={queryParams.get('sort')} name="sort" className="form-control form-select" id="" onChange={handleChangeSort}>
                      <option value="sold-descending">Top bán chạy</option>
                      <option value="title-ascending">A - Z</option>
                      <option value="title-descending">Z - A</option>
                      <option value="price-ascending">Giá thấp đến cao</option>
                      <option value="price-descending">Giá cao đến thấp</option>
                    </select>
                    <CusButton label="Reset lọc" onClick={() => handleClear()} />
                  </div>
                  <div className="d-flex align-items-center gap-10">
                    <p className="totalproducts mb-0 mx-2">{products ? products.productCount : 0} Sản Phẩm</p>
                  </div>
                </div>
              </div>
              <div className="list-product position-relative">
                {
                  products?.length === 0
                    ? <Empty />
                    : <>
                      <div style={{ visibility: `${isLoading ? 'hidden' : 'unset'}` }}>
                        <Row className="mt-3" gutter={[10, 8]}>
                          {products && products.data.map((item, index) =>
                            <Col span={6} key={index}>
                              <ProductCard data={item} />
                            </Col>
                          )}
                        </Row>
                        <Pagination
                          current={page}
                          pageSize={12}
                          onChange={changePage}
                          className="paginate"
                          total={products ? products.productCount : 0}
                        />
                      </div>
                      <Loading isLoading={isLoading} />
                    </>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Store;
