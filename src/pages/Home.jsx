import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Marquee from "react-fast-marquee";
import BlogCard from "../components/BlogCard";
import ProductCard from "../components/ProductCard";
import Meta from "../components/Meta";
import { Col, Row } from "antd";
import SlidedMultipleRows from "../components/Slider/SlidedMultipleRows";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getHotLaptop, getHotMobile } from "../redux/features/productSlice";
import { getAllCategories } from "../redux/features/categorySlice";
import { getCart, getThisUser } from "../redux/features/authSlice";
import BannerZone from "../components/BannerZone/BannerZone";
import blogService from "../redux/api/blogService";


const Home = () => {
  const dispatch = useDispatch();
  const location = useLocation()
  const navigate = useNavigate()
  const [blogs, setBlogs] = useState()

  const getAllBlogs = async () => {
    const res = await blogService.getAllBlogs()
    setBlogs(res)
  }

  const getData = async () => {
    dispatch(getHotMobile(20))
    dispatch(getHotLaptop(10))
    dispatch(getAllCategories())
    dispatch(getCart())
    getAllBlogs()
  }

  const queryParams = new URLSearchParams(location.search);

  const handleChooseCate = (name) => {
    if (name !== 'new') {
      queryParams.append('category', name)
      const newUrl = `products?${queryParams.toString()}`;
      navigate(newUrl);
    } else {
      navigate("blogs")
    }
  }

  const getUser = () => {
    dispatch(getThisUser())
  }

  useEffect(() => {
    getUser()
    getData()
  }, [])

  const { hotMobile, hotLaptop } = useSelector(state => state.product)

  const { categories } = useSelector(state => state.category)

  return (
    <>
      <Meta title="Longstore" />

      <BannerZone />

      <section className="home-wrapper-2 py-3">
        <div className="container-xxl">
          <div className="row mt-3">
            <div className="col-12">
              <div className="services d-flex align-items-center justify-content-between">
                <div className="d-flex align-items-center gap-15">
                  <img src="images/service.png" alt="" />
                  <div>
                    <h6>Miễn Phí Vận Chuyển</h6>
                    <p className="mb-0">Tất cả đơn hàng trên 2 triệu</p>
                  </div>
                </div>
                <div className="d-flex align-items-center gap-15">
                  <img src="images/service-02.png" alt="" />
                  <div>
                    <h6>Ưu Đãi Bất Ngờ Hàng Ngày</h6>
                    <p className="mb-0">Tiết kiệm tới 25%</p>
                  </div>
                </div>
                <div className="d-flex align-items-center gap-15">
                  <img src="images/service-03.png" alt="" />
                  <div>
                    <h6>Hỗ Trợ 24/7</h6>
                    <p className="mb-0">Mua sắm với một chuyên gia</p>
                  </div>
                </div>
                <div className="d-flex align-items-center gap-15">
                  <img src="images/service-04.png" alt="" />
                  <div>
                    <h6>Giá Cả Hợp Lý</h6>
                    <p className="mb-0">Đúng chi phí sản xuất</p>
                  </div>
                </div>
                <div className="d-flex align-items-center gap-15">
                  <img src="images/service-05.png" alt="" />
                  <div>
                    <h6>Thanh Toán An Toàn</h6>
                    <p className="mb-0">Được bảo vệ 100%</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/*  */}
      <section className="home-wrapper-2 py-3">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="categories">
                <Row>
                  {categories && categories.map((item, index) =>
                    <Col key={index} span={4} className="cat-border" onClick={() => handleChooseCate(item?.name)}>
                      <div className="text-center py-2">
                        <picture>
                          <img className="cat-img" src={item?.img?.url} alt="" />
                        </picture>
                        <div className="fw-bold">{item.title}</div>
                      </div>
                    </Col>)}
                </Row>
              </div>
            </div>
          </div>
        </div>
      </section>


      {/*  */}
      <section className="featured-wrapper py-2 home-wrapper-2">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <h3 className="section-heading">Điện thoại bán chạy nhất</h3>
            </div>
            <SlidedMultipleRows row={2} autoplay infinite>
              {hotMobile && hotMobile.map((item, index) =>
                <ProductCard key={index} data={item} />
              )}
              {hotMobile && hotMobile.map((item, index) =>
                <ProductCard key={index} data={item} />
              )}
            </SlidedMultipleRows>
          </div>
        </div>
      </section>

      <section className="featured-wrapper py-2 home-wrapper-2">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <h3 className="section-heading">Laptop</h3>
            </div>
          </div>
          <SlidedMultipleRows autoplay infinite>
            {hotLaptop && hotLaptop.map((item, index) =>
              <ProductCard key={index} data={item} />
            )}
            {hotLaptop && hotLaptop.map((item, index) =>
              <ProductCard key={index} data={item} />
            )}
          </SlidedMultipleRows>
        </div>
      </section>


      <section className="marque-wrapper py-2">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="marque-inner-wrapper">
                <Marquee className="d-flex">
                  <div className="mx-4 w-25">
                    <img src="images/brand-01.png" alt="" />
                  </div>
                  <div className="mx-4 w-25">
                    <img src="images/brand-02.png" alt="" />
                  </div>
                  <div className="mx-4 w-25">
                    <img src="images/brand-03.png" alt="" />
                  </div>
                  <div className="mx-4 w-25">
                    <img src="images/brand-04.png" alt="" />
                  </div>
                  <div className="mx-4 w-25">
                    <img src="images/brand-05.png" alt="" />
                  </div>
                  <div className="mx-4 w-25">
                    <img src="images/brand-06.png" alt="" />
                  </div>
                  <div className="mx-4 w-25">
                    <img src="images/brand-07.png" alt="" />
                  </div>
                  <div className="mx-4 w-25">
                    <img src="images/brand-08.png" alt="" />
                  </div>
                </Marquee>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="blog-wrapper py-4 home-wrapper-2">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <h3 className="section-heading">Tin Tức Mới Nhất</h3>
            </div>
            <div className="row">
              <SlidedMultipleRows slidesToShow={4} autoplay infinite>
                {blogs?.map(item =>
                  <div className="col-3">
                    <BlogCard data={item} />
                  </div>)
                }
              </SlidedMultipleRows>

            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Home;
