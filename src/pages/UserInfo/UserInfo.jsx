import React, { useEffect, useState } from 'react'
import Meta from '../../components/Meta'
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb'
import './UserInfo.scss'
import {
  HomeOutlined,
  UserOutlined,
  FormOutlined,
  CustomerServiceOutlined,
  LogoutOutlined,
  HistoryOutlined,
  CarryOutOutlined,
  DollarOutlined,
  SketchOutlined,
  EnvironmentOutlined
} from '@ant-design/icons';
import { Col, Form, Modal, Row, Slider, Space, Table, Tag } from 'antd'
import { useDispatch, useSelector } from 'react-redux';
import CustomInput from '../../components/CustomInput/CustomInput';
import { useForm } from 'antd/es/form/Form';
import CusButton from '../../components/CusButton/CusButton';
import { useNavigate } from 'react-router-dom';
import CustomInputSelect from '../../components/CustomInputSelect/CustomInputSelect';
import provinceService from '../../redux/api/province';
import { addAddress, getThisUser } from '../../redux/features/authSlice';
import orderService from '../../redux/api/orderService';
import { formatISODateToYYYYMMDD, formattedMoney } from '../../config/helper';
import bannerService from '../../redux/api/bannerService';
import SlidedMultipleRows from '../../components/Slider/SlidedMultipleRows';
import authService from '../../redux/api/authService';
import { toast } from 'react-toastify';

const UserInfo = () => {
  const dispatch = useDispatch()
  const { user } = useSelector(state => state.auth)
  const [current, setCurrent] = useState(1);
  const [modalAddAddress, setModalAddAddress] = useState(false)
  const [formUpdate] = useForm()
  const [formAddAddress] = useForm()
  const navigate = useNavigate()
  const [provinces, setProvinces] = useState()
  const [districts, setDistricts] = useState()
  const [wards, setWards] = useState()
  const [modalOption, setModalOption] = useState(false)
  const [modalDelete, setModalDelete] = useState(false)
  const [address, setAddress] = useState()
  const [dataOrders, setDataOrders] = useState([])
  const [modalShowOrder, setModalShowOrder] = useState(false);
  const [dataOrder, setDataOrder] = useState();
  const [listBanners, setListBanners] = useState([])

  const columns = [
    {
      title: 'Đơn hàng ngày',
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render: (text) => <span>{formatISODateToYYYYMMDD(text)}</span>,
    },
    {
      title: 'Số lượng sản phẩm',
      key: 'total',
      render: (_, record) => <span>{record.cart.quantityTotal}</span>,
      align: "center"
    },
    {
      title: 'Tồng tiền',
      key: 'address',
      render: (_, record) => <span>{formattedMoney(record.cart.cartTotal * (100 - (record.info.coupon?.discount ? record.info.coupon.discount : 0)) / 100)}₫</span>,
      align: "center"
    },
    {
      title: 'Tình trạng',
      dataIndex: 'orderStatus',
      key: 'orderStatus',
      align: "center",
      render: (text, record) => (
        <Tag color={getStatusColor(record.orderStatus)} key={text}>
          {text.toUpperCase()}
        </Tag>)
    },
    {
      title: 'Hàng động',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <span className='see-more' onClick={() => handleShowOrder(record)}>Xem chi tiết</span>
        </Space>
      ),
      align: "center"
    },
  ];

  const columnsCart = [
    {
      title: "Sản Phẩm",
      width: "35%",
      key: "title",
      render: (_, item) => (
        <div className="title">
          <span>{item.name}</span>
        </div>
      )
    },
    {
      title: "Màu sắc",
      key: "color",
      align: "center",
      render: (_, item) => (
        <span>{item.color}</span>
      )
    },
    {
      title: "Số Lượng",
      key: "quantity",
      align: "center",
      render: (_, item) => (
        <span>{item.quantity}</span>
      )
    },
    {
      title: "Thành Tiền",
      key: "subTotal",
      align: "center",
      render: (_, item) => (
        <span>{formattedMoney(item.price * item.quantity)}₫</span>
      )
    },
  ];

  const getStatusColor = (status) => {
    if (status === 'Processing') {
      return 'blue'
    }
    if (status === 'Shipping') {
      return 'blue'
    }
    if (status === 'Cancelled') {
      return 'volcano'
    }
    if (status === 'Completed') {
      return 'green'
    }
  }

  const getProvince = async () => {
    const res = await provinceService.getListProvince();
    const data = []
    res.forEach(element => {
      data.push({ id: element.province_id, value: element.province_name, label: element.province_name })
    });
    setProvinces(data)
  }

  const changeProvince = async (value) => {
    const province = provinces.find((item) => item.value === value)
    const res = await provinceService.getListDistrict(province?.id)
    const data = []
    res.forEach(element => {
      data.push({ id: element.district_id, value: element.district_name, label: element.district_name })
    });
    setDistricts(data)
    formAddAddress.resetFields(['district', 'ward'])
  }

  const changeDistrict = async (value) => {
    const district = districts.find((item) => item.value === value)
    const res = await provinceService.getListWard(district?.id)
    const data = []
    res.forEach(element => {
      data.push({ id: element.ward_id, value: element.ward_name, label: element.ward_name })
    });
    setWards(data)
    formAddAddress.resetFields(['ward'])
  }

  useEffect(() => {
    getOrders()
    getProvince()
    getAllBanners()
  }, [])

  useEffect(() => {
    if (user) {
      formUpdate.setFieldsValue({
        name: `${user.firstName} ${user.lastName}`,
        mobile: `${user.mobile}`,
        email: `${user.email}`,
        receive: 1
      })
    }
  }, [current])

  const getOrders = async () => {
    try {
      const res = await orderService.getAllOrder();
      setDataOrders(res);
    } catch (error) {
      console.log(error);
    }
  }

  const getAllBanners = async () => {
    try {
      const res = await bannerService.getAllBanners()
      setListBanners(res);
    } catch (error) {
      console.log(error);
    }
  }

  const handlUpdate = async (value) => {
    const res = await authService.updateUser(value);
    if (res?.message === "Success") {
      toast.success("Thay đổi thông tin thành công!")
    }
    dispatch(getThisUser())
  }

  const saveAddress = async (value) => {
    await dispatch(addAddress(value))
    setModalAddAddress(false)
    formAddAddress.resetFields()
  }

  const handleDelete = async () => {
    setModalDelete(true)
  }

  const handleShowOrder = async (data) => {
    await setDataOrder(data);
    setModalShowOrder(true);
  }

  const handleNagivate = (link) => {
    navigate(`${link}`)
  }

  const content = (current) => {
    switch (current) {
      case 1:
        return <>
          <Row gutter={16}>
            <Col span={12}>
              <div className='hello-user'>
                <div className='img'>
                  <img src="../../images/kafka.png" alt="" />
                </div>
                <span className='text'>Xin chào</span>
                <span className='name'>{user?.firstName} {user?.lastName}</span>
                <Row className='content'>
                  <Col span={8}>
                    <div className='item'>
                      <span>Ngày tham gia</span>
                      <CarryOutOutlined className='fs-1 red' />
                      <span>{formatISODateToYYYYMMDD(user?.createdAt)}</span>
                    </div>
                  </Col>
                  <Col span={8}>
                    <div className='item'>
                      <span>Hạng thành viên</span>
                      <SketchOutlined className='fs-1 red' />
                      <span>Coming soon</span>
                    </div>
                  </Col>
                  <Col span={8}>
                    <div className='item'>
                      <span>Điểm tích lũy</span>
                      <DollarOutlined className='fs-1 red' />
                      <span>0</span>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col span={12}>
              <div className='color'>
                <div className='block-title'>
                  <span className='title'>Chương Trình nổi bật</span>
                </div>
                <SlidedMultipleRows dots={true} slidesToShow={1} autoplay infinite>
                  {listBanners?.map(item =>
                    <div className="col-3">
                      <img onClick={() => handleNagivate(item?.action)} className='img-slide' src={item?.img?.url} alt="" />
                    </div>)
                  }
                </SlidedMultipleRows>
              </div>
            </Col>
          </Row >
        </>;
      case 2:
        return <>
          <div className='text-center mt-3'>
            <h4>Danh sách đơn hàng</h4>
            <div>
              <Table columns={columns} dataSource={dataOrders.orders} key={'id'} />
            </div>
          </div>
          <Modal className='order' width={1000} open={modalShowOrder} footer={false} onCancel={() => setModalShowOrder(false)}>
            <h4>{`Đơn hàng ${dataOrder?._id}`}</h4>
            <h5 className='mt-3'>Thông tin về đơn hàng</h5>
            <div className='d-flex gap-10'>
              <span className='label'>Tên người nhận: </span>
              <span className='content'>{dataOrder?.info.info.name}</span>
            </div>
            <div className='d-flex gap-10'>
              <span className='label'>Email: </span>
              <span className='content'>{dataOrder?.info.info.email}</span>
            </div>
            <div className='d-flex gap-10'>
              <span className='label'>Số điện thoại: </span>
              <span className='content'>{dataOrder?.info.info.phone}</span>
            </div>
            <div className='d-flex gap-10'>
              <span className='label'>Địa chỉ nhận hàng: </span>
              <span className='content'>{dataOrder?.info.info.fullAddress.province},{' '}
                {dataOrder?.info.info.fullAddress.district},{' '}
                {dataOrder?.info.info.fullAddress.ward},{' '}
                {dataOrder?.info.info.fullAddress.address}</span>
            </div>
            <div className='d-flex gap-10'>
              <span className='label'>Ngày tạo đơn hàng: </span>
              <span className='content'>{formatISODateToYYYYMMDD(dataOrder?.createdAt)}</span>
            </div>
            <div className='d-flex gap-10'>
              <span className='label'>Tình trạng đơn hàng: </span>
              <span className='content'>{dataOrder?.orderStatus}</span>
            </div>
            <div className='d-flex gap-10'>
              <span className='label'>Thanh toán: </span>
              <span className='content'>{dataOrder?.paymentIntent === "Unpaid" ? "Chưa thanh toán" : "Đã thanh toán"}</span>
            </div>
            <h5 className='mt-3'>Thông tin về sản phẩm trong đơn hàng</h5>
            <Table columns={columnsCart} dataSource={dataOrder?.cart.products} rowKey={'id'} />
            <div className='d-flex gap-10'>
              <span className='label'>Tổng giá tiền đơn hàng: </span>
              <span className='content'>{formattedMoney(dataOrder?.cart.cartTotal)}₫</span>
            </div>
            {dataOrder?.info.coupon !== null &&
              <>
                <div className='d-flex gap-10'>
                  <span className='label'>Mã giảm giá: </span>
                  <span className='content'>{dataOrder?.info.coupon.name}({dataOrder?.info.coupon.discount}%)</span>
                </div>
                <div className='d-flex gap-10'>
                  <span className='label'>Tổng giá tiền sau giảm giá: </span>
                  <span className='content'>{formattedMoney(dataOrder?.cart.cartTotal * (100 - (dataOrder?.info.coupon?.discount ? dataOrder?.info.coupon.discount : 0)) / 100)}₫</span>
                </div>
              </>
            }
          </Modal>
        </>;
      case 3:
        return <div className='info-container'>
          <div className='img'>
            <img src="../../images/kafka.png" alt="" />
          </div>
          <span className='name'>{user?.firstName} {user?.lastName}</span>
          <Form form={formUpdate} onFinish={handlUpdate} className='mt-3'>
            <div className='form-item gap-15'>
              <span>Email: </span>
              <Form.Item name='email'>
                <CustomInput disabled />
              </Form.Item>
            </div>
            <div className='form-item gap-15'>
              <span>Họ và tên: </span>
              <Form.Item name='name'
                rules={[
                  { required: true, message: "Vui lòng nhập họ và tên của bạn!" },
                ]}>
                <CustomInput />
              </Form.Item>
            </div>
            <div className='form-item gap-15'>
              <span>Số điện thoại: </span>
              <Form.Item name='mobile'
                rules={[
                  { required: true, message: "Vui lòng nhập số điện thoại của bạn!" },
                ]}>
                <CustomInput />
              </Form.Item>
            </div>
            <div className='form-item gap-15'>
              <span>Ngày tham gia: </span>
              <div className='info'>
                <span>{formatISODateToYYYYMMDD(user?.createdAt)}</span>
              </div>
            </div>
            <div className='form-item gap-15'>
              <span>Tổng tiền đã mua sắm: </span>
              <div className='info'>
                <span></span>
              </div>
            </div>
            <CusButton htmlType="submit" className='btn-update' label='Cập nhật thông tin' />
            <CusButton onClick={() => navigate('/reset-password')} className='btn-update' label='Đổi mật khẩu' />
          </Form>
        </div>;
      case 4:
        return <>
          <div className='block-address'>
            {user?.address?.map((item, index) =>
              <div key={index} className='address' onClick={() => { setModalOption(true); setAddress(item) }}>
                <img src="https://cellphones.com.vn/smember/_nuxt/img/office-building%203.3224147.png" alt="" />
                <div className='content'>
                  <span className='name'>{item.name}</span>
                  <span className='content-address'>{item.province}, {item.district}, {item.ward}, {item.address}</span>
                </div>
              </div>
            )}
          </div>
          <CusButton onClick={() => setModalAddAddress(true)} className='btn-update' label='Thêm địa chỉ mới' />
          <Modal title="Thêm địa chỉ" open={modalAddAddress} onCancel={() => { setModalAddAddress(false); formAddAddress.resetFields() }} footer={false} width={700} maskClosable={false}>
            <div className='add-address'>
              <Form form={formAddAddress} onFinish={saveAddress}>
                <Row gutter={30}>
                  <Col span={6}>Đặt tên gợi nhớ: </Col>
                  <Col span={12}>
                    <Form.Item name='name'
                      rules={[
                        { required: true, message: "Vui lòng điền tên" },
                        {
                          validator: async (_, value) => {
                            if (user?.address.some(item => item.name === value)) {
                              throw new Error("Tên này đã tồn tại");
                            }
                          },
                        },
                      ]}>
                      <CustomInput className='input' placeholder={`Ví dụ: Nhà của ${user?.lastName}`} />
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={30}>
                  <Col span={12}>
                    <Form.Item name='province'
                      rules={[
                        { required: true, message: "Vui lòng chọn Tỉnh/Thành phố" },
                      ]}>
                      <CustomInputSelect onChange={changeProvince} options={provinces} className='select' placeholder="Tỉnh/Thành phố" allowClear />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item name='district'
                      rules={[
                        { required: true, message: "Vui lòng chọn Quận/Huyện" },
                      ]}>
                      <CustomInputSelect onChange={changeDistrict} options={districts} className='select' placeholder="Quận/Huyện" allowClear />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item name='ward'
                      rules={[
                        { required: true, message: "Vui lòng chọn Phường/Xã" },
                      ]}>
                      <CustomInputSelect options={wards} className='select' placeholder="Phường/Xã" allowClear />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item name='address'
                      rules={[
                        { required: true, message: "Vui lòng điền số nhà, tên đường" },
                      ]}>
                      <CustomInput type='text' className='input' placeholder="Số nhà, tên đường" allowClear />
                    </Form.Item>
                  </Col>
                </Row>
                <CusButton type='submit' className='btn-update' label='Thêm' />
              </Form>
            </div>
          </Modal>
          <Modal title={address?.name} open={modalOption} onCancel={() => setModalOption(false)} footer={false}>
            {address?.province}, {address?.district}, {address?.ward}, {address?.address}
            <div className='d-flex justify-content-around mt-3'>
              <CusButton className="btn-option btn-update-address" label="Sửa" />
              <CusButton className="btn-option btn-delete" label="Xóa" onClick={() => handleDelete()} />
            </div>
          </Modal>
          <Modal open={modalDelete} onCancel={() => setModalDelete(false)}>
            <p>Bạn có chắc muốn xóa địa chỉ này?</p>
          </Modal>
        </>;
      default:
        return <></>;
    }
  }

  return (
    <>
      <Meta title="Thông Tin Người Dùng" />
      <BreadCrumb title="Thông Tin Người Dùng" />
      <div className='container-xxl'>
        <div className='container'>
          <div className='left-col'>
            <div className="block-menu">
              <div
                onClick={() => setCurrent(1)}
                className={`menu-item ${current === 1 ? 'active' : ''}`}>
                <HomeOutlined className='fs-4' />
                <span>Trang chủ</span>
              </div>
              <div
                onClick={() => setCurrent(2)}
                className={`menu-item ${current === 2 ? 'active' : ''}`}>
                <HistoryOutlined className='fs-4' />
                <span>Xem đơn hàng</span>
              </div>
              <div
                onClick={() => setCurrent(3)}
                className={`menu-item ${current === 3 ? 'active' : ''}`}>
                <UserOutlined className='fs-4' />
                <span>Tài khoản của bạn</span>
              </div>
              <div
                onClick={() => setCurrent(4)}
                className={`menu-item ${current === 4 ? 'active' : ''}`}>
                <EnvironmentOutlined className='fs-4' />
                <span>Thông tin địa chỉ</span>
              </div>
              <div
                onClick={() => setCurrent(5)}
                className={`menu-item ${current === 5 ? 'active' : ''}`}>
                <CustomerServiceOutlined className='fs-4' />
                <span>Hỗ trợ</span>
              </div>
              <div
                onClick={() => setCurrent(6)}
                className={`menu-item ${current === 6 ? 'active' : ''}`}>
                <FormOutlined className='fs-4' />
                <span>Góp ý - Phản hồi</span>
              </div>
              <div className='menu-item'>
                <LogoutOutlined className='fs-4' />
                <span>Thoát tài khoản</span>
              </div>
            </div>
          </div>
          <div className='right-col'>
            {content(current)}
          </div>
        </div>
      </div>
    </>
  )
}

export default UserInfo
