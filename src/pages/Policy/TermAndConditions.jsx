import React from "react";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import Meta from "../../components/Meta";

const TermAndConditions = () => {
  return (
    <>
      <Meta title="Điều Khoản Dịch Vụ" />
      <BreadCrumb title="Điều Khoản Dịch Vụ" />
    </>
  );
};

export default TermAndConditions;
