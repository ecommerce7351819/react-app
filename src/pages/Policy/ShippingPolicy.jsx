import React from "react";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import Meta from "../../components/Meta";

const ShippingPolicy = () => {
  return (
    <>
      <Meta title="Chính Sách Vận Chuyển" />
      <BreadCrumb title="Chính Sách Vận Chuyển" />
    </>
  );
};

export default ShippingPolicy;
