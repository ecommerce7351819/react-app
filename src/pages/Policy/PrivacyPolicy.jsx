import React from "react";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import Meta from "../../components/Meta";

const PrivacyPolicy = () => {
  return (
    <>
      <Meta title="Chính Sách Bảo Mật" />
      <BreadCrumb title="Chính Sách Bảo Mật" />
    </>
  );
};

export default PrivacyPolicy;
