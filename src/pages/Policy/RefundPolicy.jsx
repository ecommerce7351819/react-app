import React from "react";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import Meta from "../../components/Meta";

const RefundPolicy = () => {
  return (
    <>
      <Meta title="Chính Sách Hoàn Tiền" />
      <BreadCrumb title="Chính Sách Hoàn Tiền" />
    </>
  );
};

export default RefundPolicy;
