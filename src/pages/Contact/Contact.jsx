import React from "react";
import "./Contact.scss";
import Meta from "../../components/Meta";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";

const Contact = () => {
  return (
    <>
      <Meta title={"Liên Hệ"} />
      <BreadCrumb title="Liên Hệ" />
      <div className="py-4 home-wrapper-2 boxshadow">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <iframe
                className="map"
                title="map"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6436.30074282969!2d105.78343045846255!3d21.02540563820896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab0bf969acc7%3A0x5c1dcc77c7244bfe!2sFPT%20Tower!5e0!3m2!1sen!2s!4v1683528698504!5m2!1sen!2s"
                allowFullScreen=""
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
              ></iframe>
            </div>
            <div className="col-12 mt-5">
              <div className="contact-wrapper boxshadow d-flex justify-content-between">
                <div>
                  <h3 className="contact-title">Liên hệ</h3>
                  <form action="" className="d-flex flex-column gap-15">
                    <div>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Họ và tên"
                      />
                    </div>
                    <div>
                      <input
                        type="email"
                        className="form-control"
                        placeholder="Email"
                      />
                    </div>
                    <div>
                      <input
                        type="tel"
                        className="form-control"
                        placeholder="Số điện thoại"
                      />
                    </div>
                    <div>
                      <textarea
                        className="w-100 form-control"
                        id=""
                        cols="30"
                        rows="4"
                        placeholder="Comments"
                      ></textarea>
                    </div>
                    <div>
                      <button className="button border-0">Gửi</button>
                    </div>
                  </form>
                </div>
                <div>
                  <h3 className="contact-title">Liên lạc với chúng tôi</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Contact;
