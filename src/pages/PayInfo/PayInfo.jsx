import React, { useEffect, useState } from 'react'
import Meta from '../../components/Meta'
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb'
import { Col, Form, Modal, Radio, Row, Space, Steps, Table } from 'antd'
import { CreditCardOutlined, SolutionOutlined, ShoppingCartOutlined, InboxOutlined, PercentageOutlined } from '@ant-design/icons';
import { formattedMoney } from '../../config/helper';
import CusButton from '../../components/CusButton/CusButton';
import { useDispatch, useSelector } from 'react-redux';
import './PayInfo.scss'
import { useNavigate } from 'react-router-dom';
import CustomInput from '../../components/CustomInput/CustomInput';
import { useForm } from 'antd/es/form/Form';
import provinceService from '../../redux/api/province';
import CustomInputSelect from '../../components/CustomInputSelect/CustomInputSelect';
import { saveCoupon, saveInfoOrder } from '../../redux/features/orderSlice';
import couponService from '../../redux/api/couponService';
import { toast } from 'react-toastify';
import orderService from '../../redux/api/orderService';
import authService from '../../redux/api/authService';
import { getCart } from '../../redux/features/authSlice';

const PayInfo = () => {
  const navigate = useNavigate()
  const [form] = useForm()
  const dispatch = useDispatch()
  const [current, setCurrent] = useState(1);
  const [radio, setRadio] = useState(true);
  const [provinces, setProvinces] = useState()
  const [districts, setDistricts] = useState()
  const [wards, setWards] = useState()
  const [selectedAddress, setSelectedAddress] = useState()
  const [couponDiscount, setCouponDiscount] = useState(0)
  const [couponName, setCouponName] = useState()
  const [showListProducts, setShowListProducts] = useState(false)
  const [radioPayment, setRadioPayment] = useState(1);

  const items = [
    {
      title: 'Giỏ hàng',
      icon: <ShoppingCartOutlined />,
    },
    {
      title: 'Thông tin đặt hàng',
      icon: <SolutionOutlined />,
    },
    {
      title: 'Phiếu giảm giá',
      icon: <PercentageOutlined />,
    },
    {
      title: 'Thanh toán',
      icon: <CreditCardOutlined />,
    },
    {
      title: 'Hoàn tất đặt hàng',
      icon: <InboxOutlined />,
    },
  ]

  const columns = [
    {
      title: "Sản Phẩm",
      width: "35%",
      key: "title",
      render: (_, item) => (
        <div className="title">
          <img src={item.product.mainImg?.url} alt="" />
          <span>{item.product.title}</span>
        </div>
      )
    },
    {
      title: "Màu sắc",
      key: "color",
      align: "center",
      render: (_, item) => (
        <span>{item.color}</span>
      )
    },
    {
      title: "Số Lượng",
      key: "quantity",
      align: "center",
      render: (_, item) => (
        <span>{item.quantity}</span>
      )
    },
    {
      title: "Thành Tiền",
      key: "subTotal",
      align: "center",
      render: (_, item) => (
        <span>{formattedMoney(item.price * item.quantity)}₫</span>
      )
    },
  ];

  const onChangeStep = (value) => {
    if (value < current && value !== 0) {
      setCurrent(value);
    } else if (value === 0) (
      navigate('/cart')
    )
  };

  const onChangeRadio = (e) => {
    setRadio(e.target.value)
  }

  const onChangeAddress = (e) => {
    setSelectedAddress(e.target.value)
  }

  const handleFinish = async (value) => {
    if (current === 1) {
      await dispatch(saveInfoOrder(value));
    }
    setCurrent(current + 1)
  }

  const { user, cart } = useSelector(state => state.auth)

  const getProvince = async () => {
    const res = await provinceService.getListProvince();
    const data = []
    res.forEach(element => {
      data.push({ id: element.province_id, value: element.province_name, label: element.province_name })
    });
    setProvinces(data)
  }

  const changeProvince = async (value) => {
    const province = provinces.find((item) => item.value === value)
    const res = await provinceService.getListDistrict(province?.id)
    const data = []
    res.forEach(element => {
      data.push({ id: element.district_id, value: element.district_name, label: element.district_name })
    });
    setDistricts(data)
    form.resetFields(['district', 'ward'])
  }

  const changeDistrict = async (value) => {
    const district = districts.find((item) => item.value === value)
    const res = await provinceService.getListWard(district?.id)
    const data = []
    res.forEach(element => {
      data.push({ id: element.ward_id, value: element.ward_name, label: element.ward_name })
    });
    setWards(data)
    form.resetFields(['ward'])
  }

  useEffect(() => {
    if (user) {
      form.setFieldsValue({
        name: `${user.firstName} ${user.lastName}`,
        phone: `${user.mobile}`,
        email: `${user.email}`,
      })
    }
    getProvince()
  }, [])

  const { info, coupon } = useSelector(state => state.order)

  const handleCheckCoupon = async (value) => {
    try {
      const res = await couponService.checkCoupon(value)
      if (res?.message === "Success") {
        toast.success("Áp dụng mã giảm giá thành công!!!")
        setCouponDiscount(res?.discount)
        setCouponName(value)
        dispatch(saveCoupon({ name: value.coupon, discount: res.discount }))
      } else if (res?.message === "Expires") {
        toast.error("Mã giảm giá đã hết hạn!")
      } else {
        toast.error("Mã giảm giá không tồn tại")
      }
    } catch (error) {
      console.log(error);
    }
  }

  const createOrder = async () => {
    try {
      setCurrent(current + 2)
      const res = await orderService.createOrder({ info, coupon });
      if (res?.message === "Success") {
        await authService.emptyCart()
        await dispatch(getCart())
      }
    } catch (error) {
      console.log(error);
    }
  }

  const onChangePayment = async (e) => {
    setRadioPayment(e.target.value)
    if (e.target.value === 2) {
      orderService.createPayment()
    }
  }

  const content = (current) => {
    switch (current) {
      case 1:
        return <div>
          <Form className='form' id='myForm' form={form} onFinish={handleFinish}>
            <h6>Thông tin khách hàng</h6>
            <Form.Item name="name"
              rules={[
                { required: true, message: "Vui lòng nhập họ và tên của bạn!" },
              ]}>
              <CustomInput className="input-info" type='text' label="Họ và tên" />
            </Form.Item>
            <Form.Item name="phone"
              rules={[
                { required: true, message: "Vui lòng nhập số điện thoại của bạn!" },
              ]}>
              <CustomInput className="input-info" type='text' label="Số điện thoại" />
            </Form.Item>
            <Form.Item name="email">
              <CustomInput className="input-info" type='text' label="Email (Vui lòng điền email để nhận hoá đơn VAT)" />
            </Form.Item>
            <h6>Chọn cách thức giao hàng</h6>
            <Radio.Group onChange={onChangeRadio} value={radio} className='mb-2'>
              <Radio value={true}>Chọn nơi giao hàng</Radio>
              <Radio value={false}>Nhập nơi nhận hàng khác</Radio>
            </Radio.Group>
            {!radio ?
              <div className='address boxshadow'>
                <Row gutter={30}>
                  <Col span={12}>
                    <Form.Item name={['fullAddress', 'province']}
                      rules={[
                        { required: true, message: "Vui lòng chọn Tỉnh/Thành phố nhận hàng của bạn!" },
                      ]}>
                      <CustomInputSelect onChange={changeProvince} options={provinces} className='select' placeholder="Tỉnh/Thành phố" allowClear />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item name={['fullAddress', 'district']}
                      rules={[
                        { required: true, message: "Vui lòng chọn Quận/Huyện nhận hàng của bạn!" },
                      ]}>
                      <CustomInputSelect onChange={changeDistrict} options={districts} className='select' placeholder="Quận/Huyện" allowClear />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item name={['fullAddress', 'ward']}
                      rules={[
                        { required: true, message: "Vui lòng chọn Phường/Xã nhận hàng của bạn!" },
                      ]}>
                      <CustomInputSelect options={wards} className='select' placeholder="Phường/Xã" allowClear />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item name={['fullAddress', 'address']}
                      rules={[
                        { required: true, message: "Vui lòng chọn địa chỉ nhận hàng của bạn!" },
                      ]}>
                      <CustomInput type='text' className='input' placeholder="Số nhà, tên đường" allowClear />
                    </Form.Item>
                  </Col>
                </Row>
              </div>
              :
              <div className='choose-address'>
                <Form.Item name="fullAddress" className='mb-0'
                  rules={[
                    { required: true, message: "Vui lòng chọn địa chỉ nhận hàng của bạn!" },
                  ]}
                >
                  <Radio.Group onChange={onChangeAddress} value={selectedAddress} className='mb-2'>
                    {user?.address.map((item, index) =>
                      <div className='an-address' key={index}>
                        <Radio value={item}>{item.name} ({item.province}, {item.district}, {item.ward}, {item.address})</Radio>
                      </div>
                    )}
                  </Radio.Group>
                </Form.Item>
              </div>
            }
            <Form.Item name="req">
              <CustomInput className="input-info" type='text' label="Yêu cầu khác" />
            </Form.Item>
          </Form >
        </div >;
      case 2:
        return <div>
          <Form onFinish={handleCheckCoupon} id='check-coupon'>
            <Row>
              <Col span={20}>
                <Form.Item name="coupon">
                  <CustomInput className="input-info uppercase" type='text' label="Đừng quên sử dụng mã giảm giá mà bạn có nhé" />
                </Form.Item>
              </Col>
              <Col span={4}>
                <CusButton
                  className="btn-apply-coupon" type="submit" form="check-coupon" label="ÁP DỤNG"
                />
              </Col>
            </Row>
          </Form>
          <div className='box-info'>
            <h4>THÔNG TIN ĐẶT HÀNG</h4>
            <div className='d-flex align-items-center mb-2'>
              <span className='label'>Khách hàng:</span>
              <span className='info'>{info?.name}</span>
            </div>
            <div className='d-flex align-items-center mb-2'>
              <span className='label'>Số điện thoại:</span>
              <span className='info'>{info?.phone}</span>
            </div>
            <div className='d-flex align-items-center mb-2'>
              <span className='label'>Nhận sản phẩm tại:</span>
              <span className='info'>{info?.fullAddress.province}, {info?.fullAddress.district}, {info?.fullAddress.ward}, {info?.fullAddress.address}</span>
            </div>
            <div className='d-flex align-items-center mb-2'>
              <span className='label'>Hạng thành viên:</span>
              <span className='info'>Coming soon</span>
            </div>
            <div className='d-flex align-items-center mb-2'>
              <span className='label'>Giảm giá Smember:</span>
              <span className='info'>Coming soon</span>
            </div>
            {
              couponDiscount > 0 && <>
                <div className='d-flex align-items-center mb-2'>
                  <span className='label'>Giá khi chưa áp dụng mã giảm giá:</span>
                  <span className='info'>{formattedMoney(cart?.cartTotal)}₫</span>
                </div>
                <div className='d-flex align-items-center mb-2'>
                  <span className='label'>Áp dụng mã giảm giá:</span>
                  <span className='info'>{couponName.coupon} (-{couponDiscount}%)</span>
                </div>
                <div className='d-flex align-items-center mb-2'>
                  <span className='label'>Giá sau khi áp dụng mã giảm giá:</span>
                  <span className='info'>{formattedMoney(cart?.cartTotal - (cart?.cartTotal * (couponDiscount / 100 || 0)))}₫</span>
                </div>
              </>
            }
            <p className='see-lits-product' onClick={() => setShowListProducts(true)}>Xem danh sách sản phẩm</p>
            <Modal width={1000} title="Giỏ hàng" footer={false} open={showListProducts} onCancel={() => setShowListProducts(false)}>
              <Table className="boxshadow" columns={columns} dataSource={cart?.products} rowKey={item => item._id} pagination={false} />
            </Modal>
          </div>
        </div>;
      case 3:
        return <div>
          <h4>Chọn phương thức thanh toán</h4>
          <Radio.Group onChange={onChangePayment} value={radioPayment}>
            <Space direction="vertical">
              <Radio value={1}>Option A</Radio>
              <Radio value={2}>VNPAY</Radio>
            </Space>
          </Radio.Group>
        </div>;
      case 4:
        return <div className='completed-order'>
          <div>
            <span>Thanks!</span>
            <h4 className='my-2'>Đặt hàng thành công 🚀</h4>
            <span>Đơn hàng của bạn đang được chuyển đi. Nó sẽ được chuyển đi trong ngày hôm nay. Chúng tôi sẽ thông báo cho bạn.</span>
          </div>
          <div className='d-flex align-items-center justify-content-center my-3'>
            <img src="images/deliver.gif" alt="" />
          </div>
        </div>;
      default:
        return <></>;
    }
  }

  const checkCart = () => {
    if (cart === null) {
      navigate("/cart")
    }
  }

  useEffect(() => {
    checkCart()
  }, [])

  return (
    <>
      <Meta title="Thông Tin Đặt Hàng" />
      <BreadCrumb title="Thông Tin Đặt Hàng" />
      <div className="card-wrapper payment-info home-wrapper-2 py-4">
        <div className="container-xxl">
          <Steps
            type="navigation"
            current={current}
            onChange={onChangeStep}
            className="site-navigation-steps"
            items={items}
          />
          <div className='content mt-3 boxshadow'>
            {content(current)}
          </div>
          {current <= 3 &&
            <div className="total mt-3 boxshadow">
              {current < 3 &&
                <div className="d-flex justify-content-between">
                  <h4>Tổng tiền tạm tính</h4>
                  <span className="total-money">{formattedMoney(cart?.cartTotal - (cart?.cartTotal * (couponDiscount / 100 || 0)))}₫</span>
                </div>}
              <Row className="mt-3" gutter={16}>
                <Col span={12}>
                  {current === 2 ?
                    <CusButton
                      className="cart-btn next" label="TẠO ĐƠN HÀNG"
                      onClick={() => createOrder()}
                    />
                    :
                    <CusButton
                      type='submit' form='myForm' className="cart-btn next" label="TIẾP TỤC"
                      onClick={() => handleFinish} />
                  }
                </Col>
                <Col span={12}>
                  <CusButton onClick={() => navigate("/products")} className="cart-btn buy" label="CHỌN THÊM SẢN PHẨM KHÁC" />
                </Col>
              </Row>
            </div>}
          {
            current === 4 &&
            <div className="total mt-3 boxshadow">
              <Row className="mt-3" gutter={16}>
                <Col span={24}>
                  <CusButton onClick={() => navigate("/")} className="cart-btn next" label="VỀ TRANG CHỦ" />
                </Col>
              </Row>
            </div>
          }
        </div>
      </div>
    </>
  )
}

export default PayInfo