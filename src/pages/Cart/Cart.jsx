import React, { useEffect } from "react";
import Meta from "../../components/Meta";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import "./Cart.scss";
import { Col, Row, Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getCart } from "../../redux/features/authSlice";
import { formattedMoney } from "../../config/helper";
import CusButton from "../../components/CusButton/CusButton";
import CustomInput from "../../components/CustomInput/CustomInput";
import authService from "../../redux/api/authService";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const Cart = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const getUserCart = () => {
    dispatch(getCart())
  }

  const { cart } = useSelector(state => state.auth)

  useEffect(() => {
    getUserCart()
  }, [])

  const handleChangeQuantity = async (data, event) => {
    await authService.addToCart({ prodId: data.product._id, name: data.title, color: data.color, number: Number(event.target.value) });
    getUserCart()
  }

  const goToProduct = (slug) => {
    navigate(`/products/${slug}`)
  }

  const columns = [
    {
      title: "Sản Phẩm",
      width: "35%",
      key: "title",
      render: (_, item) => (
        <div className="title" onClick={() => goToProduct(item.slug)}>
          <img src={item.product.mainImg?.url} alt="" />
          <span>{item.product.title}</span>
        </div>
      )
    },
    {
      title: "Màu Sắc",
      dataIndex: "color",
      key: "color",
      align: "center",
    },
    {
      title: "Giá",
      key: "price",
      align: "center",
      render: (_, item) => (
        <span>{formattedMoney(item.price)}₫</span>
      )
    },
    {
      title: "Số Lượng",
      key: "quantity",
      align: "center",
      render: (_, item) => (
        <div className="quantity">
          <CustomInput max={10} min={0} onChange={(event) => handleChangeQuantity(item, event)} className='input-quantity' type="number" defaultValue={item.quantity} />
        </div>
      )
    },
    {
      title: "Thành Tiền",
      key: "subTotal",
      align: "center",
      render: (_, item) => (
        <span>{formattedMoney(item.price * item.quantity)}₫</span>
      )
    },
  ];

  return (
    <>
      <Meta title="Giỏ Hàng" />
      <BreadCrumb title="Giỏ Hàng" />
      <div className="card-wrapper home-wrapper-2 py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <Table className="boxshadow" columns={columns} dataSource={cart?.products} rowKey={item => item._id} pagination={false} />
            </div>
          </div>
          <div className="total mt-3 boxshadow">
            <div className="d-flex justify-content-between">
              <h4>Tổng tiền tạm tính</h4>
              <span className="total-money">{formattedMoney(cart?.cartTotal)}₫</span>
            </div>
            <Row className="mt-3" gutter={16}>
              <Col span={12}>
                <CusButton onClick={() => {
                  if (cart?.products.length > 0) {
                    navigate("/payment-info")
                  } else {
                    toast.error("Bạn chưa có sản phẩm trong giỏ!")
                  }
                }} className="cart-btn next" label="TIẾN HÀNH ĐẶT HÀNG" />
              </Col>
              <Col span={12}>
                <CusButton onClick={() => navigate("/products")} className="cart-btn buy" label="TIẾP TỤC MUA HÀNG" />
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </>
  );
};

export default Cart;
