import React, { useRef, useState } from "react";
import Meta from "../../components/Meta";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import "./SingleProduct.scss";
import ReactStars from "react-rating-stars-component";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import { productService } from "../../redux/api/productService";
import CusButton from "../../components/CusButton/CusButton";
import { formattedMoney } from "../../config/helper";
import SlidedMultipleRows from "../../components/Slider/SlidedMultipleRows";
import {
  SafetyCertificateOutlined, FileSyncOutlined, InboxOutlined
} from '@ant-design/icons';
import { Col, Form, Row } from "antd";
import { WARRANTY } from "../../config/constant";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../../redux/features/authSlice";

const SingleProduct = () => {
  const { slug } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [orderedProduct, setorderedProduct] = useState(true);
  const [showFormToWriteReview, setShowFormToWriteReview] = useState(false);
  const [product, setProduct] = useState();
  const [isShowMore, setIsShowMore] = useState(false);
  const [imgs, setImgs] = useState([]);
  const [img, setImg] = useState(product?.mainImg?.url);
  const [optionPrice, setOptionPrice] = useState(true)
  const [selectedColor, setSelectedColor] = useState()
  const [optionWarranty, setOptionWarranty] = useState()
  const [priceWarranty, setPriceWarranty] = useState(0)
  const [lastPrice, setLastPrice] = useState(0)

  const toCmt = useRef(null);
  const toDescription = useRef(null);

  const handleScrollToCmt = () => {
    toCmt.current?.scrollIntoView({ behavior: "smooth" })
  }

  const handleScrollToDescription = () => {
    toDescription.current?.scrollIntoView({ behavior: "smooth" })
  }

  const handleToShowForm = () => {
    setShowFormToWriteReview(!showFormToWriteReview)
  }

  const getProduct = async () => {
    try {
      const res = await productService.getAProduct(slug);
      setProduct(res);
      setImgs([res.mainImg, ...res.images]);
      const prod = res.variants.find((item) => item.quantity > 0);
      setSelectedColor(prod?.color);
      calculatorPrice(res.price, res.discount, priceWarranty);
    } catch (error) {
      console.log(error);
    }
  }

  const handleChooseWarranty = (item) => {
    setOptionWarranty(item.id === optionWarranty ? null : item.id)
    setPriceWarranty(item.percent)
  }

  useEffect(() => {
    if (!optionWarranty) {
      setPriceWarranty(0)
    }
  }, [optionWarranty])

  useEffect(() => {
    if (product) {
      calculatorPrice(product.price, product.discount, priceWarranty)
    }
  }, [priceWarranty])

  const calculatorPrice = (price, discount, priceOfWarranty) => {
    setLastPrice((price - price * (discount / 100) + price * priceOfWarranty / 100))
  }

  const { user } = useSelector(state => state.auth)

  const handleAddToCart = async () => {
    if (selectedColor !== undefined && product && user) {
      await dispatch(addToCart({ prodId: product._id, name: product.title, color: selectedColor, price: lastPrice, slug: product.slug }));
    } else
      toast.error("Bạn nên đăng nhập để đặt hàng!")
  }

  const buyNow = async () => {
    await handleAddToCart();
    navigate("/cart")
  }

  useEffect(() => {
    getProduct();
  }, [])

  const rating = async (value) => {
    try {
      await productService.rating({ prodId: product?._id, star: value.star, comment: value.comment });
      window.location.reload();
    } catch (error) { }
  }

  return (
    <>
      <Meta title={product?.title} />
      <BreadCrumb title={product?.title} />
      <div className="main-product-wrapper py-3 home-wrapper-2">
        <div className="container-xxl">
          <div className="bg-white p-3 boxshadow p-4">
            <div className="row">
              <div className="col-6">
                <div className="po-sticky">
                  <div className="border-img mb-3">
                    <img className="big-img" src={img ? img : product?.mainImg?.url} alt="" />
                  </div>
                  <SlidedMultipleRows slidesToShow={7} dots>
                    {imgs && imgs.map((item, index) => <img onClick={() => setImg(item?.url)} className="img" key={index} src={item.url} alt="" />)}
                  </SlidedMultipleRows>
                  <div className="policy">
                    <ul className="policy-list">
                      <li>
                        <div>
                          <FileSyncOutlined />
                        </div>
                        <p>Hư gì đổi nấy <b>12 tháng</b> tại 3369 siêu thị toàn quốc (miễn phí tháng đầu)</p>
                      </li>
                      <li>
                        <div>
                          <SafetyCertificateOutlined />
                        </div>
                        <p>Bảo hành <b>chính hãng điện thoại 1 năm</b> tại các trung tâm bảo hành hãng</p>
                      </li>
                      <li>
                        <div>
                          <InboxOutlined />
                        </div>
                        <p>Bộ sản phẩm gồm: Hộp, Sách hướng dẫn, Cây lấy sim, Cáp Lightning - Type C</p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-6">
                <h1 className="product-title">
                  {product?.title}
                </h1>
                <div className="d-flex gap-15 align-items-center">
                  <div className="d-flex align-items-center gap-10">
                    {product && <ReactStars
                      count={5}
                      size={20}
                      activeColor="#ffd700"
                      isHalf={true}
                      emptyIcon={<i className="far fa-star" />}
                      halfIcon={<i className="far fa-star-half-alt" />}
                      fullIcon={<i className="far fa-star" />}
                      value={product?.totalRating}
                      edit={false}
                    />}({product?.ratings.length} đánh giá)
                  </div>
                  <div className="write-cmt" onClick={handleScrollToCmt}>
                    Xem đánh giá
                  </div>
                </div>
                <div className="select-color">
                  <h5>Chọn màu</h5>
                  <div className="variants mt-3">
                    {product && product.variants?.map((item, index) => {
                      if (item.quantity > 0) {
                        return (
                          <div
                            key={index}
                            className={`variant ${selectedColor === item.color ? 'active' : ''}`}
                            onClick={() => setSelectedColor(item.color)}
                          >
                            <span>
                              {item.color}
                            </span>
                          </div>)
                      } else {
                        return (
                          <div
                            key={index}
                            className={`variant sold-out`}
                          >
                            <span>
                              {item.color}
                            </span>
                          </div>
                        )
                      }
                    })}
                  </div>
                </div>
                <Row className="option-price mt-4">
                  <Col span={12}>
                    <div className={`price-left ${optionPrice ? '' : 'price-active'}`} onClick={() => setOptionPrice(false)}>
                      <div className="icon"></div>
                      <div className="left">
                        <p className="price__show">{formattedMoney(product?.price - product?.price * (product?.discount / 100))}₫</p>
                        <p className="price__normal">Khi thu cũ lên đời</p>
                      </div>
                    </div>

                  </Col>
                  <Col span={12}>
                    <div className={`price ${optionPrice ? 'price-active' : ''}`} onClick={() => setOptionPrice(true)}>
                      <p className="price__show">{formattedMoney(lastPrice)}₫</p>
                      <p className="price__through">{formattedMoney(product?.price)}₫</p>
                    </div>
                  </Col>
                </Row>
                {optionPrice
                  ?
                  <Row className="mt-3">
                    <Col span={18}>
                      <div className="btn-buy" onClick={() => buyNow()}>
                        <strong>MUA NGAY</strong>
                        <span>(Giao nhanh từ 2 giờ hoặc nhận tại cửa hàng)</span>
                      </div>
                    </Col>
                    <Col span={6}>
                      <div className="btn-add-to-cart" onClick={() => handleAddToCart()}>
                        <img src="	https://cdn2.cellphones.com.vn/50x,webp,q70/media/wysiwyg/add-to-cart.png" alt="" />
                        <span>Thêm vào giỏ</span>
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className="btn-buy installment">
                        <strong>TRẢ GÓP 0%</strong>
                        <span>Trả trước chỉ từ 11.996.000₫</span>
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className="btn-buy installment">
                        <strong>TRẢ GÓP QUA THẺ</strong>
                        <span>(Visa, Mastercard, JCB)</span>
                      </div>
                    </Col>
                  </Row>
                  :
                  <div className="trade mt-3">
                    <ul>
                      <li>Tặng thêm 30%, tối đa <strong>2.000.000đ</strong> khi thu cũ lên đời</li>
                      <li>Tặng thêm 5%, tối đa <strong>500.000đ</strong> cho thành viên</li>
                      <li>Giá ở trên chưa bao gồm giá thu máy cũ</li>
                    </ul>
                    <Row>
                      <Col span={6}></Col>
                      <Col span={12}><div className="btn-buy">
                        <strong>LÊN ĐỜI NGAY</strong>
                      </div></Col>
                      <Col span={6}></Col>
                    </Row>
                  </div>
                }
                <div className="box-warranty-info mt-3">
                  <div className="box-header">
                    <div className="icon"><SafetyCertificateOutlined /></div>
                    <div>
                      <p className="mt-2">Bảo vệ sản phẩm toàn diện với dịch vụ bảo hành mở rộng</p>
                      <p className="note">(Khách hàng đăng ký thông tin để được hỗ trợ tư vấn và thanh toán tại cửa hàng nhanh nhất, số tiền phải thanh toán chưa bao gồm giá trị của gói bảo hành mở rộng)</p>
                    </div>
                  </div>
                  <div className="list-warranty mt-3">
                    {WARRANTY.map((item, index) =>
                      <div
                        key={index}
                        className={`warranty ${optionWarranty === item.id ? 'active' : ''}`}
                        onClick={() => handleChooseWarranty(item)}
                      >
                        <div className="title">{item.title}</div>
                        <div className="price">{formattedMoney(product?.price * item.percent / 100)}₫</div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div ref={toDescription} className="description-wrapper py-3 home-wrapper-2">
        <div className="container-xxl">
          <div className="row relative">
            <div className="col-8">
              <div className="bg-white p-3 boxshadow">
                <h4>Đặc Điểm Nổi Bật</h4>
                <div className={`description-left pb-3 ${isShowMore ? '' : 'show-less'}`} dangerouslySetInnerHTML={{ __html: product?.description }}>
                </div>
                <CusButton onClick={() => {
                  setIsShowMore(!isShowMore)
                  handleScrollToDescription()
                }} className="btn-show-more" label={isShowMore ? 'Thu Gọn' : 'Xem Thêm'} />
              </div>
            </div>
            <div className="col-4">
              <div className="bg-white p-3 boxshadow po-sticky">
                <h4>Thông Số Kỹ Thuật</h4>
                <ul className="technical-content">
                  {product && product?.specifications?.map((item, index) =>
                    <li key={index} className="technical-content-item d-flex align-items-center justify-content-between p-2">
                      <p>{item?.name}</p>
                      <div>{item?.content}</div>
                    </li>)}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section ref={toCmt} className="reviews-wrapper py-3 home-wrapper-2">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <div className="review-wrapper boxshadow">
                <div className="review-head d-flex justify-content-between align-items-end">
                  <div>
                    <h4 className="mb-2">Đánh Giá Sản Phẩm</h4>
                    <div className="d-flex align-items-center gap-10">
                      {product && <ReactStars
                        count={5}
                        size={24}
                        activeColor="#ffd700"
                        isHalf={true}
                        emptyIcon={<i className="far fa-star" />}
                        halfIcon={<i className="far fa-star-half-alt" />}
                        fullIcon={<i className="far fa-star" />}
                        value={product?.totalRating}
                        edit={false}
                      />}
                      <p className="mb-0">{product?.ratings.length} Đánh Giá</p>
                    </div>
                  </div>
                  {orderedProduct && (
                    <div onClick={handleToShowForm} className="btn-show-form">
                      {showFormToWriteReview ? 'Hủy Đánh Giá' : 'Đánh Giá Ngay'}
                    </div>
                  )}
                </div>
                {showFormToWriteReview && (
                  <div className="review-form">
                    <p className="mt-3 title">Viết Đánh Giá</p>
                    <Form onFinish={rating} className="d-flex flex-column gap-10 mt-3">
                      <div>
                        <p className="mb-0">Đánh Giá</p>
                        <Form.Item name="star">
                          <ReactStars
                            count={5}
                            size={24}
                            activeColor="#ffd700"
                            isHalf={true}
                            emptyIcon={<i className="far fa-star" />}
                            halfIcon={<i className="far fa-star-half-alt" />}
                            fullIcon={<i className="far fa-star" />}
                            value={0}
                            edit={true}
                          />
                        </Form.Item>
                      </div>
                      <div>
                        <p>Nhận Xét</p>
                        <Form.Item name="comment">
                          <textarea
                            name=""
                            id=""
                            cols="30"
                            rows="4"
                            className="w-100 form-control"
                            placeholder="Viết Nhận Xét Của Bạn Ở Đây"
                          ></textarea>
                        </Form.Item>
                      </div>
                      <div>
                        <div className="d-flex mt-2 mb-4 justify-content-end">
                          <button type="submit" className="button border-0">
                            Đăng Tải Đánh Giá
                          </button>
                        </div>
                      </div>
                    </Form>
                  </div>)}
                <div className="reviews mt-3">
                  {product?.ratings.map((item) =>
                    <div className="review mb-3">
                      <h6>{item.user}</h6>
                      <div className="comment">
                        <div className="d-flex align-items-center mb-1 gap-10">
                          <span className="title-review">Đánh Giá:</span>
                          <ReactStars
                            count={5}
                            size={24}
                            activeColor="#ffd700"
                            isHalf={true}
                            emptyIcon={<i className="far fa-star" />}
                            halfIcon={<i className="far fa-star-half-alt" />}
                            fullIcon={<i className="far fa-star" />}
                            value={item.star}
                            edit={false}
                          />
                        </div>
                        <div className="d-flex align-items-center mb-1 gap-10">
                          <p className="">
                            <span className="title-review">Nhận Xét:</span> {item.comment}
                          </p>
                        </div>
                      </div>
                    </div>)
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default SingleProduct;
