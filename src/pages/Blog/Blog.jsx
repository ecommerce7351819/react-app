import React, { useEffect, useState } from "react";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import Meta from "../../components/Meta";
import "./Blog.scss";
import BlogCard from "../../components/BlogCard";
import blogService from "../../redux/api/blogService";

const Blog = () => {
  const [blogs, setBlogs] = useState()

  const getAllBlogs = async () => {
    const res = await blogService.getAllBlogs()
    setBlogs(res)
  }

  useEffect(() => {
    getAllBlogs()
  }, [])

  return (
    <>
      <Meta title={"Blogs"} />
      <BreadCrumb title="Blogs" />
      <div className="blog-wrapper home-wrapper-2 py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-3">
              <div className="filter-card mb-3 boxshadow">

              </div>
            </div>
            <div className="col-9">
              <div className="row">
                {blogs?.map(item =>
                  <div className="col-6 mb-3">
                    <BlogCard data={item} />
                  </div>)
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Blog;
