import React from "react";
import Meta from "../../components/Meta";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import { Form } from "antd";
import CustomInput from "../../components/CustomInput/CustomInput";

const ResetPassword = () => {
  return (
    <>
      <Meta title="Đặt Lại Mật Khẩu" />
      <BreadCrumb title="Đặt Lại Mật Khẩu" />
      <div className="login-wrapper py-5 home-wrapper-2">
        <div className="auth-card">
          <h3 className="text-center mb-3">Đặt Lại Mật Khẩu</h3>
          <Form className="d-flex flex-column gap-15">
            <Form.Item name="password"
              rules={[
                { required: true, message: "Vui lòng nhập mật khẩu của bạn!" },
              ]}>
              <CustomInput
                type="password"
                label="Mật khẩu"
                id="password" />
            </Form.Item>
            <Form.Item name="password"
              rules={[
                { required: true, message: "Vui lòng nhập lại mật khẩu của bạn!" },
              ]}>
              <CustomInput
                type="password"
                label="Mật khẩu"
                id="password" />
            </Form.Item>
            <div>
              <div className="d-flex justify-content-center mt-2">
                <button className="button border-0">Đặt Lại</button>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </>
  );
};

export default ResetPassword;
