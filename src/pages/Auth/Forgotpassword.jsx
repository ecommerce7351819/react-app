import React from "react";
import "./Auth.scss";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import Meta from "../../components/Meta";
import { Link } from "react-router-dom";
import { Form } from "antd";
import CustomInput from "../../components/CustomInput/CustomInput";

const Forgotpassword = () => {
  return (
    <>
      <Meta title="Quên Mật Khẩu" />
      <BreadCrumb title="Quên Mật Khẩu" />
      <div className="login-wrapper py-5 home-wrapper-2">
        <div className="auth-card">
          <h3 className="text-center">Đặt Lại Mật Khẩu Của Bạn</h3>
          <h5 className="text-center my-4">
            Chúng tôi sẽ gửi bạn một email để cài lại mật khẩu
          </h5>
          <Form className="d-flex flex-column">
            <Form.Item name="email"
              rules={[
                { required: true, message: "Vui lòng nhập email của bạn!" },
                { type: "email", message: "Vui lòng nhập đúng định dạng email!" },
              ]}>
              <CustomInput
                type="text"
                label="Email"
                id="email" />
            </Form.Item>
            <div>
              <div className="d-flex justify-content-center">
                <button className="button border-0">Gửi</button>
              </div>
            </div>
            <div className="d-flex justify-content-center">
              <Link to="/login" className="cancel">
                Hủy
              </Link>
            </div>
          </Form>
        </div>
      </div>
    </>
  );
};

export default Forgotpassword;
