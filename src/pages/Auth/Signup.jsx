import React from "react";
import "./Auth.scss";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import Meta from "../../components/Meta";
import { Link } from "react-router-dom";
import { Form } from "antd";
import CustomInput from "../../components/CustomInput/CustomInput";
import { useDispatch } from "react-redux";
import { registerUser } from "../../redux/features/authSlice"

const Signup = () => {
  const dispatch = useDispatch()

  const handleSubmit = async (value) => {
    await dispatch(registerUser(value))
  }

  return (
    <>
      <Meta title="Tạo Tài Khoản" />
      <BreadCrumb title="Tạo Tài Khoản" />
      <div className="login-wrapper py-5 home-wrapper-2">
        <div className="auth-card">
          <h3 className="text-center mb-3">Tạo Tài Khoản</h3>
          <Form onFinish={handleSubmit} className="d-flex flex-column gap-15">
            <Form.Item name="email"
              rules={[
                { required: true, message: "Vui lòng nhập email của bạn!" },
                { type: "email", message: "Vui lòng nhập đúng định dạng email!" },
              ]}>
              <CustomInput
                type="text"
                label="Email"
                id="email"
              />
            </Form.Item>
            <Form.Item name="firstName"
              rules={[
                { required: true, message: "Vui lòng nhập họ của bạn của bạn!" },
              ]}>
              <CustomInput
                type="text"
                label="Họ của bạn"
                id="firstName"
              />
            </Form.Item>
            <Form.Item name="lastName"
              rules={[
                { required: true, message: "Vui lòng nhập tên của bạn của bạn!" },
              ]}>
              <CustomInput
                type="text"
                label="Tên của bạn"
                id="lastName"
              />
            </Form.Item>
            <Form.Item name="mobile"
              rules={[
                { required: true, message: "Vui lòng nhập số điện thoại của bạn của bạn!" },
              ]}>
              <CustomInput
                type="text"
                label="Số điện thoại"
                id="mobile"
              />
            </Form.Item>
            <Form.Item name="password"
              rules={[
                { required: true, message: "Vui lòng nhập mật khẩu của bạn!" },
              ]}>
              <CustomInput
                type="password"
                label="Mật khẩu"
                id="password"
              />
            </Form.Item>
            <div>
              <div className="d-flex justify-content-center mt-2 gap-15">
                <button type="submit" className="button border-0">Đăng Ký</button>
              </div>
              <div className="d-flex justify-content-center mt-3">
                <Link to="/login" className="link-to">
                  Quay Trở Về Đăng Nhập
                </Link>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </>
  );
};

export default Signup;
