import React, { useEffect } from "react";
import "./Auth.scss";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import Meta from "../../components/Meta";
import { Link, useNavigate } from "react-router-dom";
import { Form } from "antd";
import CustomInput from "../../components/CustomInput/CustomInput";
import { useDispatch, useSelector } from "react-redux";
import { loginUser } from "../../redux/features/authSlice";

const Login = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleSubmit = (value) => {
    dispatch(loginUser(value))
  }

  const { message } = useSelector(state => state.auth)

  useEffect(() => {
    if (message === "Success") {
      navigate('/')
    }
  }, [message])

  return (
    <>
      <Meta title="Đăng Nhập" />
      <BreadCrumb title="Đăng Nhập" />
      <div className="login-wrapper py-5 home-wrapper-2">
        <div className="auth-card">
          <h3 className="text-center mb-3">Đăng Nhập</h3>
          <Form onFinish={handleSubmit} className="d-flex flex-column gap-15">
            <Form.Item name="email"
              rules={[
                { required: true, message: "Vui lòng nhập email của bạn!" },
                { type: "email", message: "Vui lòng nhập đúng định dạng email!" },
              ]}>
              <CustomInput
                type="text"
                label="Email"
                id="email" />
            </Form.Item>
            <Form.Item name="password"
              rules={[
                { required: true, message: "Vui lòng nhập mật khẩu của bạn!" },
              ]}>
              <CustomInput
                type="password"
                label="Mật khẩu"
                id="password" />
            </Form.Item>
            <div>
              <Link to="/forgot-password" className="forgot-password fw-bold">
                Quên Mật Khẩu?
              </Link>
              <div className="d-flex justify-content-center gap-15 align-items-between my-3">
                <button type="submit" className="button border-0">Đăng Nhập</button>
              </div>
              <div className="d-flex justify-content-center gap-5">
                Bạn chưa có tài khoản?
                <Link to="/signup" className="link-to">
                  Đăng Ký Ngay
                </Link>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </>
  );
};

export default Login;
